/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExtPropertySheetComponent.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extpropertysheet.ui;

import org.openesb.tools.extpropertysheet.IExtPropertyGroupsBean;
import com.sun.web.ui.component.TabSet;
import java.io.IOException;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.el.ValueBinding;

/**
 *
 * @author rdwivedi
 */
public class ExtPropertySheetComponent extends UIComponentBase {
    
    private static Logger mLogger = Logger.getLogger(ExtPropertySheetComponent.class.getName());
    public static final String COMPONENT_TYPE = "org.openesb.tools.extpropertysheet.ui.ExtPropertySheetComponent";
	
    public static final String COMPONENT_FAMILY = "org.openesb.tools.extpropertysheet.ui";
    
    /** Creates a new instance of ExtPropertySheetComponent */
    
    private IExtPropertyGroupsBean mPropGroups = null;
    private String mResBundle = null;
   
    public ExtPropertySheetComponent() {
        
    }

    public String getFamily() {
        return COMPONENT_FAMILY;
    }
    
    public void encodeBegin(FacesContext context) throws IOException { 
        mPropGroups =(IExtPropertyGroupsBean) getPropertyGroups();
       // preEncoding(context);
        ResponseWriter writer = context.getResponseWriter();
        writer.startElement("div", this);
        
    }
    public void encodeEnd(FacesContext context) throws IOException {
        context.getResponseWriter().endElement("div");
    }
    
    public void createSubComponent(FacesContext context) {
        UIComponent comp = null;
        mPropGroups = (IExtPropertyGroupsBean)getPropertyGroups();
        mResBundle = (String)getResourceBundle();
        SubComponentCreator cr = new SubComponentCreator(mResBundle);
        if(comp == null) {
            comp = cr.createTabSet(mPropGroups,context);
        }
        this.getChildren().add(comp);
        
    }
    
    
    public Object getPropertyGroups() {
        if(mPropGroups != null)
            return mPropGroups;
       
        ValueBinding vb = getValueBinding("propertyGroups");
        Object v = vb != null ? vb.getValue(getFacesContext()) : null;
        
        return v != null ? v: null;
    }
    
    public void setPropertyGroups(Object pGs) {
        
        this.mPropGroups = (IExtPropertyGroupsBean)pGs;
    }

    public Object getResourceBundle() {
        if(mResBundle != null)
            return mResBundle;
       
        ValueBinding vb = getValueBinding("resourceBundle");
        Object v = vb != null ? vb.getValue(getFacesContext()) : null;
        return v != null ? v: null;
    }
    
    public void setResourceBundle(Object pGs) {
        mResBundle = (String)pGs;
    }

    public void decode(FacesContext context) {
        super.decode(context);
        mPropGroups = (IExtPropertyGroupsBean)getPropertyGroups();
        PropertiesUpdater up = new PropertiesUpdater();
        up.updateChartProperties((TabSet)this.getChildren().get(0),mPropGroups);
        
    }
     

    public void restoreState(FacesContext context, Object state) {
        super.restoreState(context,state);
        
          
    }

     
    
    public void processDecodes(FacesContext context) {
        super.processDecodes(context);
        
    }
    
}

    
