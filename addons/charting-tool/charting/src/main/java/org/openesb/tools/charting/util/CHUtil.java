/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)CHUtil.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.charting.util;

/**
 *
 * @author rdwivedi
 */
public class CHUtil {
    
    /** Creates a new instance of CHUtil */
    public CHUtil() {
    }
    
  /* 
    
    public void doPost(HttpServletRequest request, 
        HttpServletResponse response) throws IOException {
        // Whatever processing is needed
        //...
        LifecycleFactory lFactory = (LifecycleFactory)
        FactoryFinder.getFactory(FactoryFinder.LIFECYCLE_FACTORY);
        Lifecycle lifecycle =
            lFactory.getLifecycle(LifecycleFactory.DEFAULT_LIFECYCLE);

       FacesContextFactory fcFactory = (FacesContextFactory)
       FactoryFinder.getFactory(FactoryFinder.FACES_CONTEXT_FACTORY);
       FacesContext context =
           fcFactory.getFacesContext(context, request, response, lifecycle);

       Application application = context.getApplication( );
       ViewHandler viewHandler = application.getViewHandler( );
       UIViewRoot view = viewHandler.createView(facesContext, "/myView.jsp");
       facesContext.setViewRoot(view);

       lifecycle.render( );
    }
   * */
}
