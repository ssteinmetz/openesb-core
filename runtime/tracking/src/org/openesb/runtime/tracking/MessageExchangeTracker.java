/*
  * BEGIN_HEADER - DO NOT EDIT

  * The contents of this file are subject to the terms
  * of the Common Development and Distribution License
  * (the "License").  You may not use this file except
  * in compliance with the License.

  * You can obtain a copy of the license at
  * http://opensource.org/licenses/cddl1.php.
  * See the License for the specific language governing
  * permissions and limitations under the License.

  * When distributing Covered Code, include this CDDL
  * HEADER in each file and include the License file at
  * http://opensource.org/licenses/cddl1.php.
  * If applicable add the following below this CDDL HEADER,
  * with the fields enclosed by brackets "[]" replaced with
  * your own identifying information: Portions Copyright
  * [year] [name of copyright owner]
 */

package org.openesb.runtime.tracking;


import javax.jbi.messaging.MessageExchange;
import org.openesb.runtime.tracking.tasks.MessageTrackingTask;
import org.openesb.runtime.tracking.tasks.MessageTrackingTaskFactory;

/**
 *
 * Copyright 2011 Alexander Lomov.
 */

/**
 * This class is a message exchange routing recorder: when was the exchange
 * created, what exchange has spawned it, at what time, etc.
 * Every ME state change should be propagated.
 *
 */
public class MessageExchangeTracker {

    public static final String TRACKING_ID = "MESSAGE_TRACKING_ID";

    public static void track(MessageTrackingInfo trackingInfo){
        MessageTrackingTask task = MessageTrackingTaskFactory.get().getTask(trackingInfo);
        task.track();
    }

}
