<?xml version="1.0" encoding="UTF-8"?>
<!--
 # BEGIN_HEADER - DO NOT EDIT
 #
 # The contents of this file are subject to the terms
 # of the Common Development and Distribution License
 # (the "License").  You may not use this file except
 # in compliance with the License.
 #
 # You can obtain a copy of the license at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # See the License for the specific language governing
 # permissions and limitations under the License.
 #
 # When distributing Covered Code, include this CDDL
 # HEADER in each file and include the License file at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # If applicable add the following below this CDDL HEADER,
 # with the fields enclosed by brackets "[]" replaced with
 # your own identifying information: Portions Copyright
 # [year] [name of copyright owner]
-->

<!--
 # @(#)esb-core.xsd
 # Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 #
 # END_HEADER - DO NOT EDIT
-->
<xs:schema xmlns:jbi="http://java.sun.com/xml/ns/jbi/management-message" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://java.sun.com/xml/ns/jbi/esb" targetNamespace="http://java.sun.com/xml/ns/jbi/esb" elementFormDefault="qualified">
   <xs:import namespace="http://java.sun.com/xml/ns/jbi/management-message" schemaLocation="managementMessage.xsd"/>
   <!-- Generic parameter definition -->
   <xs:complexType name="ParameterType">
      <xs:sequence>
         <xs:element name="name" type="xs:string"/>
         <xs:element name="value" type="xs:string"/>
      </xs:sequence>
   </xs:complexType>
   <xs:complexType name="InstanceType">
      <xs:sequence>
         <xs:element ref="name"/>
         <xs:element ref="domain" minOccurs="0"/>
         <xs:element ref="host"/>
         <xs:element ref="port"/>
         <xs:element ref="user"/>
         <xs:element ref="password"/>
         <xs:element ref="description" minOccurs="0"/>
         <xs:element ref="connection-state"/>
         <xs:element ref="parameters"/>
         <xs:element ref="log-info" minOccurs="0" maxOccurs="1"/>
      </xs:sequence>
   </xs:complexType>
   <xs:complexType name="ComponentType">
      <xs:sequence>
         <xs:element ref="name"/>
         <xs:element ref="description" minOccurs="0"/>
         <xs:element ref="component-type"/>
         <xs:element ref="archive-info"/>
         <xs:element name="component-details">
            <xs:complexType>
               <xs:sequence>
                  <xs:element name="component-detail" type="ComponentDetailType" minOccurs="0" maxOccurs="unbounded"/>
               </xs:sequence>
            </xs:complexType>
         </xs:element>
      </xs:sequence>
   </xs:complexType>
   <xs:complexType name="SharedLibraryType">
      <xs:sequence>
         <xs:element ref="name"/>
         <xs:element ref="description" minOccurs="0"/>
         <xs:element ref="archive-info"/>
         <xs:element name="shared-library-details">
            <xs:complexType>
               <xs:sequence>
                  <xs:element name="shared-library-detail" type="SharedLibraryDetailType" minOccurs="0" maxOccurs="unbounded"/>
               </xs:sequence>
            </xs:complexType>
         </xs:element>
         <xs:element name="dependent-components-list" type="NameListType"/>
      </xs:sequence>
   </xs:complexType>
   <xs:complexType name="ServiceAssemblyType">
      <xs:sequence>
         <xs:element ref="name"/>
         <xs:element ref="description" minOccurs="0"/>
         <xs:element ref="archive-info"/>
         <xs:element name="service-assembly-details">
            <xs:complexType>
               <xs:sequence>
                  <xs:element name="service-assembly-detail" type="ServiceAssemblyDetailType" minOccurs="0" maxOccurs="unbounded"/>
               </xs:sequence>
            </xs:complexType>
         </xs:element>
         <xs:element ref="service-units" minOccurs="0"/>
      </xs:sequence>
   </xs:complexType>
   <xs:complexType name="ServiceUnitType">
      <xs:sequence>
         <xs:element ref="name"/>
         <xs:element ref="description" minOccurs="0"/>
         <xs:element ref="archive-info"/>
         <xs:element ref="target-component"/>
         <xs:element name="service-unit-details">
            <xs:complexType>
               <xs:sequence>
                  <xs:element name="service-unit-detail" type="ServiceUnitDetailType" minOccurs="0" maxOccurs="unbounded"/>
               </xs:sequence>
            </xs:complexType>
         </xs:element>
      </xs:sequence>
   </xs:complexType>
   <xs:complexType name="NameListType">
      <xs:sequence>
         <xs:element ref="name" minOccurs="0" maxOccurs="unbounded"/>
      </xs:sequence>
   </xs:complexType>
   <xs:complexType name="DetailType">
      <xs:sequence>
         <xs:element name="instance-name" type="xs:string"/>
      </xs:sequence>
   </xs:complexType>
   <xs:complexType name="SharedLibraryDetailType">
      <xs:complexContent>
         <xs:extension base="DetailType">
            <xs:sequence>
               <xs:element name="install-timestamp" type="xs:dateTime"/>
            </xs:sequence>
         </xs:extension>
      </xs:complexContent>
   </xs:complexType>
   <xs:complexType name="ComponentDetailType">
      <xs:complexContent>
         <xs:extension base="DetailType">
            <xs:sequence>
               <xs:element name="install-timestamp" type="xs:dateTime"/>
               <xs:element ref="life-cycle-state"/>
            </xs:sequence>
         </xs:extension>
      </xs:complexContent>
   </xs:complexType>
   <xs:complexType name="ServiceAssemblyDetailType">
      <xs:complexContent>
         <xs:extension base="DetailType">
            <xs:sequence>
               <xs:element name="deploy-timestamp" type="xs:dateTime"/>
               <xs:element ref="life-cycle-state"/>
            </xs:sequence>
         </xs:extension>
      </xs:complexContent>
   </xs:complexType>
   <xs:complexType name="ServiceUnitDetailType">
      <xs:complexContent>
         <xs:extension base="DetailType">
            <xs:sequence>
               <xs:element name="deploy-timestamp" type="xs:dateTime"/>
               <xs:element ref="life-cycle-state"/>
            </xs:sequence>
         </xs:extension>
      </xs:complexContent>
   </xs:complexType>
   <!-- Enumerations -->
   <xs:simpleType name="LifeCycleStatusEnum">
      <xs:restriction base="xs:string">
         <xs:enumeration value="STARTED"/>
         <xs:enumeration value="STOPPED"/>
         <xs:enumeration value="SHUTDOWN"/>
         <xs:enumeration value="UNKNOWN"/>
      </xs:restriction>
   </xs:simpleType>
   <xs:simpleType name="ConnectionStatusEnum">
      <xs:restriction base="xs:string">
         <xs:enumeration value="PENDING"/>
         <xs:enumeration value="STARTING"/>
         <xs:enumeration value="SYNCHRONIZING"/>
         <xs:enumeration value="CONNECTED"/>
         <xs:enumeration value="DISCONNECTED"/>
      </xs:restriction>
   </xs:simpleType>
   <!-- Common element definitions -->
   <xs:element name="archive-info">
      <xs:complexType>
         <xs:sequence>
            <xs:element ref="name"/>
            <xs:element ref="size"/>
            <xs:element name="upload-timestamp" type="xs:dateTime"/>
            <xs:element name="jbi-xml-timestamp" type="xs:dateTime"/>
         </xs:sequence>
      </xs:complexType>
   </xs:element>
   <xs:element name="component-type">
      <xs:simpleType>
         <xs:restriction base="xs:string">
            <xs:enumeration value="service-engine"/>
            <xs:enumeration value="binding-component"/>
         </xs:restriction>
      </xs:simpleType>
   </xs:element>
   <xs:element name="life-cycle-state">
      <xs:complexType>
         <xs:sequence>
            <xs:element name="desired" type="LifeCycleStatusEnum"/>
            <xs:element name="actual" type="LifeCycleStatusEnum" minOccurs="0"/>
         </xs:sequence>
      </xs:complexType>
   </xs:element>
   <xs:element name="parameters">
      <xs:complexType>
         <xs:sequence>
            <xs:element name="parameter" type="ParameterType" minOccurs="0" maxOccurs="unbounded"/>
         </xs:sequence>
      </xs:complexType>
   </xs:element>
   <xs:element name="service-units">
      <xs:complexType>
         <xs:sequence>
            <xs:element name="service-unit" type="ServiceUnitType" minOccurs="0" maxOccurs="unbounded"/>
         </xs:sequence>
      </xs:complexType>
   </xs:element>
   
   <xs:element name="log-info">
        <xs:complexType>
            <xs:sequence>
                <xs:element name="sync-time" type="xs:dateTime" minOccurs="0" maxOccurs="1"/>
                <xs:element name="index" type="xs:unsignedLong"/>
                <xs:element name="sync-index" type="xs:unsignedLong"/>
                <xs:element ref="log-item" minOccurs="0" maxOccurs="unbounded"/>
            </xs:sequence>
        </xs:complexType>
    </xs:element>

    <xs:element name="log-item">
        <xs:complexType>
            <xs:sequence>
                <xs:element name="index" type="xs:unsignedLong"/>
               <xs:element name="action" type="xs:string"/>
               <xs:element name="target" type="xs:string"/>
               <xs:element name="arg" type="xs:string" minOccurs="0" maxOccurs="1"/>
               <xs:element name="extra" type="xs:string" minOccurs="0" maxOccurs="1"/>
            </xs:sequence>
       </xs:complexType>
   </xs:element>
   
   <xs:element name="connection-state" type="ConnectionStatusEnum"/>
   <xs:element name="description" type="xs:string"/>
   <xs:element name="user" type="xs:string"/>
   <xs:element name="password" type="xs:string"/>
   <xs:element name="host" type="xs:string"/>
   <xs:element name="port" type="xs:string"/>
   <xs:element name="domain" type="xs:string"/>
   <xs:element name="name" type="xs:string"/>
   <xs:element name="target-component" type="xs:string"/>
   <xs:element name="size" type="xs:unsignedLong"/>
</xs:schema>
