/*
 * XML Type:  DescriptionType
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.DescriptionType
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl;


/**
 * An XML DescriptionType(@http://www.w3.org/ns/wsdl).
 *
 * This is a complex type.
 */
public interface DescriptionType extends org.w3.ns.wsdl.ExtensibleDocumentedType
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(DescriptionType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s2EAECD9BB08C57F25B7B261051DD8E7E").resolveHandle("descriptiontypef1e7type");
    
    /**
     * Gets array of all "import" elements
     */
    org.w3.ns.wsdl.ImportType[] getImportArray();
    
    /**
     * Gets ith "import" element
     */
    org.w3.ns.wsdl.ImportType getImportArray(int i);
    
    /**
     * Returns number of "import" element
     */
    int sizeOfImportArray();
    
    /**
     * Sets array of all "import" element
     */
    void setImportArray(org.w3.ns.wsdl.ImportType[] ximportArray);
    
    /**
     * Sets ith "import" element
     */
    void setImportArray(int i, org.w3.ns.wsdl.ImportType ximport);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "import" element
     */
    org.w3.ns.wsdl.ImportType insertNewImport(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "import" element
     */
    org.w3.ns.wsdl.ImportType addNewImport();
    
    /**
     * Removes the ith "import" element
     */
    void removeImport(int i);
    
    /**
     * Gets array of all "include" elements
     */
    org.w3.ns.wsdl.IncludeType[] getIncludeArray();
    
    /**
     * Gets ith "include" element
     */
    org.w3.ns.wsdl.IncludeType getIncludeArray(int i);
    
    /**
     * Returns number of "include" element
     */
    int sizeOfIncludeArray();
    
    /**
     * Sets array of all "include" element
     */
    void setIncludeArray(org.w3.ns.wsdl.IncludeType[] includeArray);
    
    /**
     * Sets ith "include" element
     */
    void setIncludeArray(int i, org.w3.ns.wsdl.IncludeType include);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "include" element
     */
    org.w3.ns.wsdl.IncludeType insertNewInclude(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "include" element
     */
    org.w3.ns.wsdl.IncludeType addNewInclude();
    
    /**
     * Removes the ith "include" element
     */
    void removeInclude(int i);
    
    /**
     * Gets array of all "types" elements
     */
    org.w3.ns.wsdl.TypesType[] getTypesArray();
    
    /**
     * Gets ith "types" element
     */
    org.w3.ns.wsdl.TypesType getTypesArray(int i);
    
    /**
     * Returns number of "types" element
     */
    int sizeOfTypesArray();
    
    /**
     * Sets array of all "types" element
     */
    void setTypesArray(org.w3.ns.wsdl.TypesType[] typesArray);
    
    /**
     * Sets ith "types" element
     */
    void setTypesArray(int i, org.w3.ns.wsdl.TypesType types);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "types" element
     */
    org.w3.ns.wsdl.TypesType insertNewTypes(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "types" element
     */
    org.w3.ns.wsdl.TypesType addNewTypes();
    
    /**
     * Removes the ith "types" element
     */
    void removeTypes(int i);
    
    /**
     * Gets array of all "interface" elements
     */
    org.w3.ns.wsdl.InterfaceType[] getInterfaceArray();
    
    /**
     * Gets ith "interface" element
     */
    org.w3.ns.wsdl.InterfaceType getInterfaceArray(int i);
    
    /**
     * Returns number of "interface" element
     */
    int sizeOfInterfaceArray();
    
    /**
     * Sets array of all "interface" element
     */
    void setInterfaceArray(org.w3.ns.wsdl.InterfaceType[] xinterfaceArray);
    
    /**
     * Sets ith "interface" element
     */
    void setInterfaceArray(int i, org.w3.ns.wsdl.InterfaceType xinterface);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "interface" element
     */
    org.w3.ns.wsdl.InterfaceType insertNewInterface(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "interface" element
     */
    org.w3.ns.wsdl.InterfaceType addNewInterface();
    
    /**
     * Removes the ith "interface" element
     */
    void removeInterface(int i);
    
    /**
     * Gets array of all "binding" elements
     */
    org.w3.ns.wsdl.BindingType[] getBindingArray();
    
    /**
     * Gets ith "binding" element
     */
    org.w3.ns.wsdl.BindingType getBindingArray(int i);
    
    /**
     * Returns number of "binding" element
     */
    int sizeOfBindingArray();
    
    /**
     * Sets array of all "binding" element
     */
    void setBindingArray(org.w3.ns.wsdl.BindingType[] bindingArray);
    
    /**
     * Sets ith "binding" element
     */
    void setBindingArray(int i, org.w3.ns.wsdl.BindingType binding);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "binding" element
     */
    org.w3.ns.wsdl.BindingType insertNewBinding(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "binding" element
     */
    org.w3.ns.wsdl.BindingType addNewBinding();
    
    /**
     * Removes the ith "binding" element
     */
    void removeBinding(int i);
    
    /**
     * Gets array of all "service" elements
     */
    org.w3.ns.wsdl.ServiceType[] getServiceArray();
    
    /**
     * Gets ith "service" element
     */
    org.w3.ns.wsdl.ServiceType getServiceArray(int i);
    
    /**
     * Returns number of "service" element
     */
    int sizeOfServiceArray();
    
    /**
     * Sets array of all "service" element
     */
    void setServiceArray(org.w3.ns.wsdl.ServiceType[] serviceArray);
    
    /**
     * Sets ith "service" element
     */
    void setServiceArray(int i, org.w3.ns.wsdl.ServiceType service);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "service" element
     */
    org.w3.ns.wsdl.ServiceType insertNewService(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "service" element
     */
    org.w3.ns.wsdl.ServiceType addNewService();
    
    /**
     * Removes the ith "service" element
     */
    void removeService(int i);
    
    /**
     * Gets the "targetNamespace" attribute
     */
    java.lang.String getTargetNamespace();
    
    /**
     * Gets (as xml) the "targetNamespace" attribute
     */
    org.apache.xmlbeans.XmlAnyURI xgetTargetNamespace();
    
    /**
     * Sets the "targetNamespace" attribute
     */
    void setTargetNamespace(java.lang.String targetNamespace);
    
    /**
     * Sets (as xml) the "targetNamespace" attribute
     */
    void xsetTargetNamespace(org.apache.xmlbeans.XmlAnyURI targetNamespace);
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.w3.ns.wsdl.DescriptionType newInstance() {
          return (org.w3.ns.wsdl.DescriptionType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.w3.ns.wsdl.DescriptionType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.w3.ns.wsdl.DescriptionType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.w3.ns.wsdl.DescriptionType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.w3.ns.wsdl.DescriptionType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.w3.ns.wsdl.DescriptionType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.w3.ns.wsdl.DescriptionType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.w3.ns.wsdl.DescriptionType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.DescriptionType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.w3.ns.wsdl.DescriptionType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.DescriptionType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.w3.ns.wsdl.DescriptionType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.DescriptionType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.w3.ns.wsdl.DescriptionType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.DescriptionType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.w3.ns.wsdl.DescriptionType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.DescriptionType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.w3.ns.wsdl.DescriptionType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.DescriptionType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.w3.ns.wsdl.DescriptionType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.DescriptionType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.w3.ns.wsdl.DescriptionType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.DescriptionType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.w3.ns.wsdl.DescriptionType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.w3.ns.wsdl.DescriptionType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.w3.ns.wsdl.DescriptionType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.w3.ns.wsdl.DescriptionType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.w3.ns.wsdl.DescriptionType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.w3.ns.wsdl.DescriptionType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.w3.ns.wsdl.DescriptionType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.w3.ns.wsdl.DescriptionType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link XMLInputStream} */
        public static org.w3.ns.wsdl.DescriptionType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.w3.ns.wsdl.DescriptionType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link XMLInputStream} */
        public static org.w3.ns.wsdl.DescriptionType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.w3.ns.wsdl.DescriptionType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
