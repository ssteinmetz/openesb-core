/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import java.io.StringWriter;

import java.util.Map;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.XmlOptions;

import org.w3.ns.wsdl.EndpointType;
import org.w3.ns.wsdl.ServiceType;

import org.w3c.dom.DocumentFragment;

/**
 * Implementation of WSDL 2.0 Service component.
 * 
 * @author Sun Microsystems, Inc.
 */
final class ServiceImpl extends Service
{
    /** The container for this component */
    private DescriptionImpl    mContainer;
  
    /**
     * Get the container for this component.
     * 
     * @return The component for this component
     */
    protected DescriptionImpl getContainer()
    {
        return this.mContainer;
    }
  
    /**
     * Construct a Service component implementation from the given XML bean.
     * @param bean The Service XML bean to use to construct this component.
     * @param defs The container for this component.
     */
    private ServiceImpl(ServiceType bean, DescriptionImpl defs)
    {
        super(bean);
        this.mContainer = defs;
    }
  
    /** Map of WSDL-defined attribute QNames. Keyed by QName.toString value */
    private static java.util.Map sWsdlAttributeQNames = null;

    /** 
     * Worker class method for {@link #getWsdlAttributeNameMap()}.
     * 
     * @return Map of WSDL-defined attribute QNames for this component, 
     *         indexed by QName.toString()
     */
    static synchronized java.util.Map getAttributeNameMap()
    {
        if (sWsdlAttributeQNames == null)
        {
            sWsdlAttributeQNames = XmlBeansUtil.getAttributesMap(ServiceType.type);
        }

        return sWsdlAttributeQNames;
    }
  
    /**
     * Get map of WSDL-defined attribute QNames for this component, indexed by 
     * canonical QName string (see {@link javax.xml.namespace.QName#toString()}.
     *
     * @return Map of WSDL-defined attribute QNames for this component, 
     *         indexed by QName.toString()
     */
    public java.util.Map getWsdlAttributeNameMap()
    {
        return getAttributeNameMap();
    }
  
    /**
     * Get the qualified name of this service.
     * 
     * @return The qualified name of this service.
     */
    public QName getQName()
    {
        return new QName(this.mContainer.getTargetNamespace(), getName());
    }
  
    /**
     * Get local name of this service component.
     *
     * @return Local name of this service component
     */
    public String getName()
    {
        return getBean().getName();
    }

    /**
     * Set local name of this service component.
     *
     * @param theName Local name of this service component
     */
    public void setName(String theName)
    {
        getBean().setName(theName);
    }

    /**
     * Get target namespace of this service.
     *
     * @return Target namespace of this service
     */
    public String getTargetNamespace()
    {
        return this.mContainer.getTargetNamespace();
    }

    /**
     * Get interface provided by this service.
     *
     * @return Interface provided by this service
     */
    public com.sun.jbi.wsdl2.Interface getInterface()
    {
        QName   name  = getBean().getInterface();

        return this.mContainer.findInterface(name);
    }

    /**
     * Set interface provided by this service.
     *
     * @param theInterface Interface provided by this service
     */
    public void setInterface(com.sun.jbi.wsdl2.Interface theInterface)
    {
        QName     name = null;
    
        if (theInterface != null)
        {
            name = new QName(
                theInterface.getTargetNamespace(),
                theInterface.getName());
        }

        // assert (this.mContainer.getTargetNamespace() == name.getNamespaceUri());

        getBean().setInterface(name);
    }

    /**
     * Get the number of Endpoint items in endpoints.
     *
     * @return The number of Endpoint items in endpoints
     */
    public int getEndpointsLength()
    {
        return getBean().sizeOfEndpointArray();
    }

    /**
     * Get endpoints for this service by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Endpoints for this service at given <code>index</code> position.
     */
    public com.sun.jbi.wsdl2.Endpoint getEndpoint(int index)
    {
        return EndpointImpl.Factory.getInstance(
            getBean().getEndpointArray(index),
            this.mContainer);
    }

    /**
     * Set endpoints for this service by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theEndpoint Item to add at position <code>index</code>.
     */
    public void setEndpoint(
        int index, 
        com.sun.jbi.wsdl2.Endpoint theEndpoint)
    {
        getBean().setEndpointArray(
            index,
            theEndpoint != null ? ((EndpointImpl) theEndpoint).getBean() : null);
    }

    /**
     * Append an item to endpoints for this service.
     *
     * @param theEndpoint Item to append to endpoints
     */
    public void appendEndpoint(com.sun.jbi.wsdl2.Endpoint theEndpoint)
    {
        synchronized (getBean().monitor())
        {
            setEndpoint(getEndpointsLength(), theEndpoint);
        }
    }

    /**
     * Remove endpoints for this service by index position.
     *
     * @param index The index position of the endpoint to remove
     * @return The Endpoint removed, if any.
     */
    public com.sun.jbi.wsdl2.Endpoint removeEndpoint(int index)
    {
        com.sun.jbi.wsdl2.Endpoint    result;

        synchronized (getBean().monitor())
        {
            result = getEndpoint(index);
            getBean().removeEndpoint(index);
        }

        return result;
    }

    /**
     * Create a new end point component, appending it to this service's endpoint list.
     *
     * @param name NC name for the new endpoint.
     * @param binding Binding to which the endpoint refers.
     * @return The newly created endpoint, appended to this service's endpoint list.
     */
    public com.sun.jbi.wsdl2.Endpoint addNewEndpoint(String name, 
        com.sun.jbi.wsdl2.Binding binding)
    {
        EndpointType    epBean = getBean().addNewEndpoint();

        synchronized (epBean.monitor())
        {
            epBean.setName(name);
    
            if (binding != null)
            {
                epBean.setBinding(((BindingImpl) binding).getQName());
            }
        }

        return EndpointImpl.Factory.getInstance(
            epBean,
            this.mContainer);
    }

    /**
     * Return this WSDL service as an XML string.
     *
     * @return This service, serialized as an XML string.
     */
    public String toXmlString()
    {
        String          result; 
        StringWriter    sw      = new StringWriter();
        XmlOptions      options = new XmlOptions();

        options.setSavePrettyPrint();
        options.setSavePrettyPrintIndent(Constants.XML_PRETTY_PRINT_INDENT);
        options.setSaveOuter();

        try
        {
            getBean().save(sw, options);
            sw.close();
        }
        catch (java.io.IOException ex)
        {
            sw.write( "\n<!-- IO error: " );
            sw.write( ex.getMessage() );
            sw.write( "\n     Document fragment truncated. -->\n" );
            // $TODO: log error
        }
        
        return sw.getBuffer().toString();
    }

    /**
     * Return this service as a DOM document fragment.
     *
     * @return This service, as a DOM document fragment.
     */
    public DocumentFragment toXmlDocumentFragment()
    {
        XmlOptions  options = new XmlOptions();

        options.setSaveOuter();
        return (DocumentFragment) getBean().newDomNode(options);
    }
  
    /**
     * A factory class for creating / finding components for given XML beans.
     * <p>
     * This factory guarantees that there will only be one component for each
     * XML bean instance.
     */
    static class Factory
    {
        /**
         * Find the WSDL service component associated with the given XML
         * bean, creating a new component if necessary.
         * <p>
         * This is thread-safe.<p>
         * 
         * @param bean The XML bean to find the component for.
         * @param defs The container for the component.
         * @return The WSDL service component for the given <code>bean</code>
         *         (null if the <code>bean</code> is null).
         */
        static ServiceImpl getInstance(ServiceType bean, DescriptionImpl defs)
        {
            ServiceImpl result;

            if (bean != null)
            {
                Map     map = defs.getServiceMap();
                
                synchronized (map)
                {
                    result = (ServiceImpl) map.get(bean);

                    if (result == null)
                    {
                        result = new ServiceImpl(bean, defs);
                        map.put(bean, result);
                    }
                }
            }
            else
            {
                result = null;
            }

            return result;
        }
    }
}

// End-of-file: ServiceImpl.java
