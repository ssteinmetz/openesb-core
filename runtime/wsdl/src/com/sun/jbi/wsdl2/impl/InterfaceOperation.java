/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InterfaceOperation.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import javax.xml.namespace.QName;
import org.w3c.dom.DocumentFragment;
import org.w3.ns.wsdl.InterfaceOperationType;

/**
 * Abstract implementation of
 * WSDL 2.0 interface operation component.
 *
 * @author Sun Microsystems, Inc.
 */
abstract class InterfaceOperation extends ExtensibleDocumentedComponent
    implements com.sun.jbi.wsdl2.InterfaceOperation
{
    /**
     * Get the Xml bean for this component.
     *
     * @return The Xml bean for this component.
     */
    protected final InterfaceOperationType getBean()
    {
        return (InterfaceOperationType) this.mXmlObject;
    }

    /**
     * Construct an abstract Interface operation implementation base component.
     * 
     * @param bean      The XML bean for this Interface operation component
     */
    InterfaceOperation(InterfaceOperationType bean)
    {
        super(bean);
    }

    /**
     * Create a new input message reference for this operation, and append it
     * to this operation's input list.
     *
     * @return The newly created input message reference.
     */
    public abstract com.sun.jbi.wsdl2.MessageReference addNewInput();

    /**
     * Create a new output message reference for this operation, and append
     * it to this operation's output list.
     *
     * @return The newly created input message reference.
     */
    public abstract com.sun.jbi.wsdl2.MessageReference addNewOutput();

    /**
     * Create a new messsage in-fault reference for this operation, and
     * append it to the operation's in-fault list.
     *
     * @param fault Fault referenced by the new in-fault
     * @return The newly created in-fault reference.
     */
    public abstract com.sun.jbi.wsdl2.MessageFaultReference addNewInFault(
        com.sun.jbi.wsdl2.InterfaceFault fault);

    /**
     * Create a new messsage out-fault reference for this operation, and
     * append it to the operation's out-fault list.
     *
     * @param fault Fault referenced by the new out-fault
     * @return The newly created out-fault reference.
     */
    public abstract com.sun.jbi.wsdl2.MessageFaultReference addNewOutFault(
        com.sun.jbi.wsdl2.InterfaceFault fault);

    /**
     * Return this WSDL interface operation as an XML string.
     *
     * @return This operation, serialized as an XML string.
     */
    public abstract String toXmlString();

    /**
     * Return this interface operation as a DOM document fragment. The DOM
     * subtree is a copy; altering it will not affect this service.
     *
     * @return This operation, as a DOM document fragment.
     */
    public abstract DocumentFragment toXmlDocumentFragment();

}

// End-of-file: InterfaceOperation.java
