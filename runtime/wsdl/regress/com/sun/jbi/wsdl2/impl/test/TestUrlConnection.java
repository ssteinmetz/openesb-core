/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestUrlConnection.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl.test;

import com.sun.jbi.wsdl2.Constants;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.net.URL;

/**
 * This class serves as a test URL protocol connection.
 * 
 * @author Sun Microsystems Inc.
 */
public class TestUrlConnection extends java.net.URLConnection
{
    /** For backward compatibility test (for x2006x01) */
    private static final String
        WSDL_NAMESPACE_NAME_2006_01 = "http://www.w3.org/2006/01/wsdl";

    /**
     * Construct a test connection to the given URL.
     * 
     * @param url the URL of the resource to connect to.
     */
    TestUrlConnection(URL url)
    {
        super(url);

        this.doInput = true;
        this.setUseCaches(false);
    }
        
    /**
     * Opens a communications link to the resource referenced by 
     * this connection's URL, if such a connection has not already been 
     * established.
     * 
     * @exception IOException if an I/O error occurs while opening the
     *                        connection.
     */
    public void connect() throws IOException
    {
        if (!this.connected)
        {
            this.connected = true;
        }
    }

    /** A simple, empty WSDL description document */
    private static final String TEST1 = 
        "<description xmlns = '" + Constants.WSDL_NAMESPACE_NAME + "'\n" +
        "             targetNamespace='http://foo.com/ns'>\n" +
        "</description>\n";

    /** An illegal WSDL definition (wrong namespace) */
    private static final String TEST2 =
        "<description xmlns = 'http://wrong-namespace.com/'\n" +
        "             targetNamespace='http://foo.com/ns>'>\n" +
        "</description>\n";

    /** An illegal WSDL definition (ill-formed) */
    private static final String TEST3 =
        "<description xmlns = '" + Constants.WSDL_NAMESPACE_NAME + "'\n" +
        "             targetNamespace='http://foo.com/ns'>\n" +
        "  <service>\n" +
        "</description>\n";

    /** An illegal WSDL definition (invalid) */
    private static final String TEST4 =
        "<description xmlns = '" + Constants.WSDL_NAMESPACE_NAME + "'\n" +
        "             targetNamespace='http://foo.com/ns'>\n" +
        "  <operation/>\n" +
        "</description>\n";

    /** A simple, empty WSDL description document, but for 
      * backward compatibility test */
    private static final String TEST5 =
        "<description xmlns = '" + WSDL_NAMESPACE_NAME_2006_01 + "'\n" +
        "             targetNamespace='http://foo.com/ns'>\n" +
        "</description>\n";

    /**
     * Get an input stream for this connection.
     * 
     * @return an input stream for this connection.
     */
    public InputStream getInputStream()
    {
        String       file = getURL().getFile();
        String       body = "<bad-test-url/>";

        if ("/notwsdl.wsdl".equalsIgnoreCase(file))
        {
            body = "<foo/>";
        }
        else if ("/test1.wsdl".equalsIgnoreCase(file))
        {
            body = TEST1;
        }
        else if ("/test2.wsdl".equalsIgnoreCase(file))
        {
            body = TEST2;
        }
        else if ("/test3.wsdl".equalsIgnoreCase(file))
        {
            body = TEST3;
        }
        else if ("/test4.wsdl".equalsIgnoreCase(file))
        {
            body = TEST4;
        }
        else if ("/test5.wsdl".equalsIgnoreCase(file))
        {
            body = TEST5;
        }

        return new ByteArrayInputStream(body.getBytes());
    }
}
