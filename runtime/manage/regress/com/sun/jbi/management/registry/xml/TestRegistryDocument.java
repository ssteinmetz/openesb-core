/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestRegistryDocument.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.registry.xml;

import com.sun.jbi.management.ComponentInfo;
import com.sun.jbi.management.ConfigurationCategory;
import com.sun.jbi.management.system.Util;
import com.sun.jbi.management.registry.RegistryBuilder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import org.w3c.dom.Document;

public class TestRegistryDocument 
    extends junit.framework.TestCase
{
    /**
     * The sample Configuration Directory.
     */
    private String mConfigDir = null;
    private File mRegFile;
    private Document mRegistryDocument;

    String mRegFilePath;
    String mRegGoodFilePath;

    
    public TestRegistryDocument (String aTestName)
    {
        super(aTestName);
        
        String srcroot = System.getProperty("junit.srcroot");
        String manage = "/runtime/manage";        // open-esb build
        mConfigDir = srcroot + manage + "/bld/test-classes/testdata/";

        java.io.File f = new java.io.File(srcroot + manage);
        if (! f.exists())
        {
            manage = "/shasta/manage";       // mainline/whitney build
            mConfigDir = srcroot + manage + "/bld/regress/testdata/";
        }
        
        mRegFilePath        = mConfigDir + File.separator + "jbi-registry.xml";
        mRegGoodFilePath    = mConfigDir + File.separator + "jbi-registry-good.xml";
        
        mRegFile = new File(mRegFilePath);
        
    }

    public void setUp()
        throws Exception
    {
        super.setUp();
        
        Util.fileCopy(mRegGoodFilePath, mRegFilePath );
        
        if (mRegFile.canRead())
        {        
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            mRegistryDocument = db.parse(mRegFile);
        }
    }

    public void tearDown()
        throws Exception
    {


    }

    /**
     * Test getting a configuration attribute value
     */
    public void testGetConfigurationAttribute()
           throws Exception
    {
        RegistryDocument regDoc = new RegistryDocument(mRegistryDocument);
        
        String value = regDoc.getConfigurationAttribute(
                            "domain", 
                            ConfigurationCategory.Installation,
                            "componentTimeout");
        assertEquals("5000", value);
    }
    
    /**
     * Test getting a configuration attribute value
     */
    public void testGetOverridenConfigurationAttribute()
           throws Exception
    {
        RegistryDocument regDoc = new RegistryDocument(mRegistryDocument);
        
        String value = regDoc.getConfigurationAttribute(
                            "clusterA", 
                            ConfigurationCategory.Installation,
                            "componentTimeout");
        assertEquals("4000", value);
    }
    
    /**
     * Test getting all the attributes in a category
     */
    public void testGetConfigurationAttributes()
           throws Exception
    {
        RegistryDocument regDoc = new RegistryDocument(mRegistryDocument);
        
        java.util.Map<String, String> values = regDoc.getConfigurationAttributes(
                            "domain", 
                            ConfigurationCategory.Deployment);
        assertTrue(values.get("startOnDeploy").equals("true"));
        assertTrue(values.get("serviceUnitTimeout").equals("5000"));
        assertTrue(values.get("deploymentTimeout").equals("6000"));
    }
    
    /**
     * Test getting all the attributes in a category for a target, which has some
     * overrides
     */
    public void testGetOverridenConfigurationAttributes()
           throws Exception
    {
        RegistryDocument regDoc = new RegistryDocument(mRegistryDocument);
        
        java.util.Map<String, String> values = regDoc.getConfigurationAttributes(
                            "clusterA", 
                            ConfigurationCategory.Deployment);

        assertTrue(values.get("startOnDeploy").equals("false"));
    }
    
    /**
     * Test getting a missing configuration attribute.
     */
    public void testGetMissingConfigurationAttribute()
           throws Exception
    {
        RegistryDocument regDoc = new RegistryDocument(mRegistryDocument);
        
        String value = regDoc.getConfigurationAttribute(
                            "domain", 
                            ConfigurationCategory.Installation,
                            "xyz");
        assertNull(value);
    }   
    
    /**
     * Test getting component configuration attributes.
     */
    public void testGetComponentConfigurationAttributes()
    {
        RegistryDocument regDoc = new RegistryDocument(mRegistryDocument);
        
        java.util.Properties props = regDoc.getComponentConfigurationAttributes(
                            "server", 
                            true,
                            "SunSequencingEngine");
        assertNotNull(props);
        assertEquals(props.get("HostName"), "tango");
        assertEquals(props.get("Port"), "5656");
    }
    
    /**
     * Test getting component application variables.
     */
    public void testGetComponentApplicationVariables()
    {
        RegistryDocument regDoc = new RegistryDocument(mRegistryDocument);
        
        ComponentInfo.Variable[] vars = regDoc.getComponentApplicationVariables(
                            "server", 
                            true,
                            "SunSequencingEngine");
        assertNotNull(vars);
        assertEquals(2, vars.length);
        ComponentInfo.Variable var = vars[0];
        
        assertEquals(var.getName(), "name");
        assertEquals(var.getValue(), "Gill Bates");
        assertEquals(var.getType(), "STRING");
    }
    
    /**
     * Test getting component application configuration.
     */
    public void testGetComponentApplicationConfiguration()
    {
        RegistryDocument regDoc = new RegistryDocument(mRegistryDocument);
        
        java.util.Map<String, java.util.Properties> configMap = regDoc.getComponentApplicationConfiguration(
                            "server", 
                            true,
                            "SunSequencingEngine");
        assertTrue(configMap.containsKey("SEQ_CONFIG1"));
        assertTrue(configMap.containsKey("SEQ_CONFIG2"));
        
        // -- Test whether the property values are read correctly
        java.util.Properties config1 = configMap.get("SEQ_CONFIG1");
        assertTrue(config1.getProperty("configurationName").equals("SEQ_CONFIG1"));
        assertTrue(config1.getProperty("initialContextFactory").equals("com.sonicsw.jndi.mfcontext.MFContextFactory"));
        assertTrue(config1.getProperty("connectionURL").equals("jndi://cfg1"));
        
        
        java.util.Properties config2 = configMap.get("SEQ_CONFIG2");
        assertTrue(config2.getProperty("configurationName").equals("SEQ_CONFIG2"));
        assertTrue(config2.getProperty("initialContextFactory").equals("com.sonicsw.jndi.mfcontext.MFContextFactory"));
        assertTrue(config2.getProperty("connectionURL").equals("jndi://cfg2")); 
    }
    
}
