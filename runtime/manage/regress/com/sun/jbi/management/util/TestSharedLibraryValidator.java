/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestSharedLibraryValidator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.util;

import com.sun.jbi.management.system.ScaffoldEnvironmentContext;
import com.sun.jbi.management.system.Util;
import java.io.File;

public class TestSharedLibraryValidator 
    extends junit.framework.TestCase
{

    private String mConfigDir = null;
    private String GOOD_SL               = "wsdlsl.jar";
    private String SL_MISSING_CLASS      = "wsdlsl-missing-classpath.zip";
    private String SL_MISSING_CLASS_2    = "wsdlsl-missing-classpath2.zip";
    private Validator mValidator;

    public TestSharedLibraryValidator (String aTestName)
        throws Exception
    {
        super(aTestName);
        
        String srcroot = System.getProperty("junit.srcroot");
        String manage = "/runtime/manage";        // open-esb build
        mConfigDir = srcroot + manage + "/bld/test-classes/testdata/";

        java.io.File f = new java.io.File(srcroot + manage);
        if (! f.exists())
        {
            manage = "/shasta/manage";       // mainline/whitney build
            mConfigDir = srcroot + manage + "/bld/regress/testdata/";
        }
        
        
        ValidatorFactory.setDescriptorValidation(true);
        ScaffoldEnvironmentContext envCtx = new ScaffoldEnvironmentContext();
        envCtx.setJbiInstallRoot(System.getProperty("junit.as8base") + "/jbi");
        com.sun.jbi.util.EnvironmentAccess.setContext(envCtx);

        
        //this is a bad dependency on ManagementContext. 
        //This should be fixed when validation logic is removed from Archive
        Util.createManagementContext();        
        
        mValidator = ValidatorFactory.createValidator(envCtx,
			ValidatorType.SHARED_LIBRARY);
      
    }

    public void setUp()
        throws Exception
    {
        super.setUp();
    }

    public void tearDown()
        throws Exception
    {
    }

    
    /**
     * Test with a good shared library
     */
    public void testValidateSharedLibraryGood()
           throws Exception
    {
        File testComp = new File(mConfigDir, GOOD_SL); 
        
        try
        {
            mValidator.validate(testComp);
        }
        catch ( Exception ex )
        {
            fail("Unexpected exception " + ex.getMessage());
        }
    }
    
    /**
     * Test with a bad shared library missing classpath elements
     */
    public void testValidateSharedLibraryMissingClasspath()
           throws Exception
    {
        File testComp = new File(mConfigDir, SL_MISSING_CLASS); 
        
        try
        {
            mValidator.validate(testComp);
        }
        catch ( Exception ex )
        {
            assertTrue(ex.getMessage().contains("JBI_ADMIN_EMPTY_SHARED_LIBRARY_CLASSPATH"));
        }
    }
    
    
    /**
     * Test with a bad shared library missing classpath elements
     */
    public void testValidateSharedLibraryMissingClasspath2()
           throws Exception
    {
        File testComp = new File(mConfigDir, SL_MISSING_CLASS_2); 
        
        try
        {
            mValidator.validate(testComp);
        }
        catch ( Exception ex )
        {
            assertTrue(ex.getMessage().contains("JBI_ADMIN_ARCHIVE_DESCRIPTOR_NOT_SCHEMA_VALID"));
        }
    }
    
}
