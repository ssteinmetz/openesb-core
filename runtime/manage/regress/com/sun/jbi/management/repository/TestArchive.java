/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestArchive.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.repository;

import java.io.File;
import com.sun.jbi.management.system.Util;

public class TestArchive extends junit.framework.TestCase
{
    private static final String COMPONENT_ARCHIVE_PATH =
            "/testdata/component.zip";
    
    private static final String BAD_COMPONENT_ARCHIVE_PATH =
            "/testdata/bad-component.zip";
    
    private static final String SERVICE_ASSEMBLY_ARCHIVE_PATH =
            "/testdata/service-assembly.zip";
        
    private static final String SCHEMA_INVALID_ARCHIVE_PATH =
            "/testdata/sa-schema-invalid.zip";
        
    private static final String COMPONENT_NAME = 
            "SunSequencingEngine";
    
    private static final String SERVICE_ASSEMBLY_NAME = 
            "CompositeApplication";
    
    private static final String SERVICE_UNIT_1_NAME = 
            "ESB_ADMIN_SERVICE_UNIT_1";
    
    private static final String SERVICE_UNIT_2_NAME = 
            "ESB_ADMIN_SERVICE_UNIT_2";
    
    private static final String SERVICE_UNIT_1_PATH =
            "su1.jar";
    
    private static final String SERVICE_UNIT_2_PATH =
            "su2.jar";
    
    private File mComponentArchive;
    private File mBadComponentArchive;
    private File mServiceAssemblyArchive;
    private File mSchemaInvalidArchive;
            
    public TestArchive (String aTestName)
    {
        super(aTestName);
        
        try
        {
            String srcRoot = System.getProperty("junit.srcroot");
            String manage = "/runtime/manage";        // open-esb build
            String regress = "/bld/test-classes";         // open-esb build

            java.io.File f = new java.io.File(srcRoot + manage);
            if (! f.exists())
            {
                manage = "/shasta/manage";       // mainline/whitney build
                regress = "/bld/regress";        // mainline/whitney build
            }
            srcRoot = srcRoot + manage + regress;

            mComponentArchive       = new File(srcRoot, COMPONENT_ARCHIVE_PATH);
            mBadComponentArchive    = new File(srcRoot, BAD_COMPONENT_ARCHIVE_PATH);
            mServiceAssemblyArchive = new File(srcRoot, SERVICE_ASSEMBLY_ARCHIVE_PATH);
            mSchemaInvalidArchive   = new File(srcRoot, SCHEMA_INVALID_ARCHIVE_PATH);
            
            //this is a bad dependency on ManagementContext. 
            //This should be fixed when validation logic is removed from Archive
            Util.createManagementContext();        
        
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
    
    public void setUp()
        throws Exception
    {
        super.setUp();
        
    }

    public void tearDown()
        throws Exception
    {

    }
    
    public void testNewComponentArchive()
        throws Exception
    {
        Archive component;
        
        component = new Archive(mComponentArchive, true);
        
        assertTrue(component.getType() == ArchiveType.COMPONENT);
        assertTrue(component.getJbiName().equals(COMPONENT_NAME));
    }
    
    public void testNewComponentArchiveMissingJbiXml()
        throws Exception
    {
        Archive component;
        
        try
        {
            component = new Archive(mBadComponentArchive, true);
            fail("Created archive from invalid source -- missing jbi.xml");
        }
        catch (RepositoryException rEx) {}
    }
    
    public void testNewServiceAssemblyArchive()
        throws Exception
    {
        Archive assembly;
        
        assembly = new Archive(mServiceAssemblyArchive, true);
        
        // verify SA details
        assertTrue(assembly.getType() == ArchiveType.SERVICE_ASSEMBLY);
        assertTrue(assembly.getJbiName().equals(SERVICE_ASSEMBLY_NAME));
        
        // verify SU details
        assertTrue(assembly.hasChildren());
        assertEquals(assembly.getChildPath(SERVICE_UNIT_1_NAME), SERVICE_UNIT_1_PATH);
        assertEquals(assembly.getChildPath(SERVICE_UNIT_2_NAME), SERVICE_UNIT_2_PATH);
    }
    
    public void testSchemaInvalidSA()
        throws Exception
    {
        Archive component;
        
        try
        {
            component = new Archive(mSchemaInvalidArchive, true);
            fail("Created archive from invalid source -- schema invalid");
        }
        catch (RepositoryException rEx) {}
    }
}
