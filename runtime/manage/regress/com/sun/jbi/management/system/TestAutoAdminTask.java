/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestAutoAdminTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.system;

import java.io.File;
import javax.management.ObjectName;
import javax.management.StandardMBean;

import com.sun.jbi.management.MBeanNames;

import com.sun.jbi.management.system.AutoAdminTask;
import com.sun.jbi.management.system.ManagementContext;
import com.sun.jbi.management.system.ScaffoldEnvironmentContext;

import com.sun.jbi.management.util.FileHelper;

/**
 * Tests some methods in AutoAdminTask
 * @author Sun Microsystems, Inc.
 */

public class TestAutoAdminTask
    extends junit.framework.TestCase
{

    /**
     * Current test name.
     */
    private String mTestName;

    /**
     * Helper class to hold created Environment Context
     */
    private ScaffoldEnvironmentContext mContext;

    /**
     * Helper class to hold management context
     */
    private ManagementContext mCtx;

    /**
     * the actual installation configuration service MBean
     */
    private ObjectName mInstallConfigSvcMBean;

    /**
     * the actual deployment configuration service MBean
     */
    private ObjectName mDeployConfigSvcMBean;

    /**
     * The AutoAdminTask class being tested
     */
    private AutoAdminTask autoAdminTask;

    /**
     * A temporary directory to use for autoinstall
     */
    private String mAutoInstallDir;

    /**
     * A temporary directory to use for autoinstall
     */
    private String mAutoDeployDir;

    /**
     * Holders of various files created during the directory population
     */
    static private File newF1;  // these two only exist in the top directory
    static private File newF2;
    static private File deletedF1; // these two only exist in the status dir.
    static private File deletedF2;
    static private File oldF1;     // these exist in both directories, with
    static private File tsF1;      // the same timestamp.
    static private File oldF2;
    static private File tsF2;
    static private File updatedF1; // these exist in both directories, with
    static private File updatedF2; // the top version "newer" the the status
    static private File tsuF1;     // directory version.
    static private File tsuF2;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestAutoAdminTask(String aTestName) throws Exception
    {
        super(aTestName);
        mTestName = aTestName;
    }
    
    /**
     * Setup for the test.
     * @throws Exception when setup fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        System.err.println("***** Start of test " + mTestName);

        // create and set up the Environment Context
        mContext = new ScaffoldEnvironmentContext();

        mCtx = new ManagementContext(mContext);

        mInstallConfigSvcMBean = mCtx.getMBeanNames().getSystemServiceMBeanName(
            MBeanNames.ServiceName.ConfigurationService,
            MBeanNames.ServiceType.Installation,
            mContext.getPlatformContext().getInstanceName());

        mDeployConfigSvcMBean = mCtx.getMBeanNames().getSystemServiceMBeanName(
            MBeanNames.ServiceName.ConfigurationService,
            MBeanNames.ServiceType.Deployment,
            mContext.getPlatformContext().getInstanceName());

        mAutoInstallDir = System.getProperty("junit.srcroot") + "/bld/autoinstall";
        mAutoDeployDir = System.getProperty("junit.srcroot") + "/bld/autodeploy";

        autoAdminTask = new AutoAdminTask(mCtx, mAutoInstallDir, mAutoDeployDir, true, false);
    }

    /**
     * Cleanup for the test.
     * @throws Exception when cleanup fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        System.err.println("***** End of test " + mTestName);
    }
    
// =============================== test methods ===============================

    /*
     * Test that autoDirectories does not create directories when told not to.
     * @throws Exception when test fails for any reason.
     */
    public void testInitDirectoriesFalseFalse()
        throws Exception
    {
        File instDir = new File(mAutoInstallDir);
        assertFalse("auto install directory(" + mAutoInstallDir + ") already exists.",
                    instDir.exists());
        
        autoAdminTask.initDirectories(false, false);
        assertFalse("auto install directory(" + mAutoInstallDir + ") was created.",
                    instDir.exists());
    }

    /*
     * Test that autoDirectories does not create directories when told not to.
     * @throws Exception when test fails for any reason.
     */
    public void testInitDirectoriesTrueFalse()
        throws Exception
    {
        File instDir = new File(mAutoInstallDir);
        File deplDir = new File(mAutoDeployDir);
        assertFalse("auto install directory(" + mAutoInstallDir + ") already exists.",
                    instDir.exists());
        assertFalse("auto deploy directory(" + mAutoDeployDir + ") already exists.",
                    deplDir.exists());
        
        autoAdminTask.initDirectories(true, false);
        assertTrue("auto install directory(" + mAutoInstallDir + ") was not created.",
                    instDir.exists());
        assertFalse("auto deploy directory(" + mAutoDeployDir + ") was wrongly created.",
                    deplDir.exists());
    }

    /*
     * Test that autoDirectories does not create directories when told not to.
     * @throws Exception when test fails for any reason.
     */
    public void testInitDirectoriesFalseTrue()
        throws Exception
    {
        File instDir = new File(mAutoInstallDir);
        File deplDir = new File(mAutoDeployDir);
        assertTrue("auto install directory(" + mAutoInstallDir + ") doesn't exist.",
                    instDir.exists());
        assertFalse("auto deploy directory(" + mAutoDeployDir + ") already exists.",
                    deplDir.exists());
        
        autoAdminTask.initDirectories(false, true);
        assertTrue("auto install directory(" + mAutoInstallDir + ") was deleted.",
                    instDir.exists());
        assertTrue("auto install directory(" + mAutoInstallDir + ") was not created.",
                    deplDir.exists());
    }

    /*
     * Test that pollAutoDirectory returns null when no files are present.
     * @throws Exception when test fails for any reason.
     */
    public void testPollAutoDirectoryNewEmpty()
        throws Exception
    {
        File instDir = new File(mAutoInstallDir);
        File statusDir = new File(mAutoInstallDir, ".autoinstallstatus");
        assertTrue(".autoinstallstatus directory does not exist.",
                    statusDir.exists());
        
        File results = autoAdminTask.pollAutoDirectory(
                           AutoAdminTask.PollFunction.NEW_FILES, instDir, statusDir);
        assertNull("pollAutoDirectory returned a non-null file", results);
    }

    /*
     * Test that pollAutoDirectory returns null when no files are present.
     * @throws Exception when test fails for any reason.
     */
    public void testPollAutoDirectoryDeletedEmpty()
        throws Exception
    {
        File instDir = new File(mAutoInstallDir);
        File statusDir = new File(mAutoInstallDir, ".autoinstallstatus");
        assertTrue(".autoinstallstatus directory does not exist.",
                    statusDir.exists());
        
        File results = autoAdminTask.pollAutoDirectory(
                           AutoAdminTask.PollFunction.DELETED_FILES, instDir, statusDir);
        assertNull("pollAutoDirectory returned a non-null file", results);
    }

    /*
     * Test that pollAutoDirectory returns null when no files are present.
     * @throws Exception when test fails for any reason.
     */
    public void testPollAutoDirectoryUpdatedEmpty()
        throws Exception
    {
        File instDir = new File(mAutoInstallDir);
        File statusDir = new File(mAutoInstallDir, ".autoinstallstatus");
        assertTrue(".autoinstallstatus directory does not exist.",
                    statusDir.exists());
        
        File results = autoAdminTask.pollAutoDirectory(
                           AutoAdminTask.PollFunction.UPDATED_FILES, instDir, statusDir);
        assertNull("pollAutoDirectory returned a non-null file", results);
    }

    /*
     * Test that pollAutoDirectory returns the right files under a complex
     * scenario with new and processed files in place.
     * @throws Exception when test fails for any reason.
     */
    public void testPollAutoDirectoryNewFilled()
        throws Exception
    {
        File instDir = new File(mAutoInstallDir);
        File statusDir = new File(mAutoInstallDir, ".autoinstallstatus");
        assertTrue(".autoinstallstatus directory does not exist.",
                    statusDir.exists());
        
        // create some "new" files
        newF1 = new File(instDir, "new1.jar");
        assertTrue ("could not create new file1", newF1.createNewFile());
        newF2 = new File(instDir, "new2.jar");
        assertTrue ("could not create new file2", newF2.createNewFile());

        // create some "deleted" files
        deletedF1 = new File(statusDir, "deleted1.jar");
        assertTrue ("could not create deleted file1", deletedF1.createNewFile());
        deletedF2 = new File(statusDir, "deleted2.jar");
        assertTrue ("could not create deleted file2", deletedF2.createNewFile());

        // create some "old" (already installed) files
        oldF1 = new File(instDir, "old1.jar");
        assertTrue ("could not create old file1", oldF1.createNewFile());
        tsF1 = new File(statusDir, "old1.jar");
        assertTrue ("could not create timestamp file1", tsF1.createNewFile());
        oldF2 = new File(instDir, "old2.jar");
        assertTrue ("could not create old file2", oldF2.createNewFile());
        tsF2 = new File(statusDir, "old2.jar");
        assertTrue ("could not create timestamp file2", tsF2.createNewFile());
        tsF1.setLastModified(oldF1.lastModified());
        tsF2.setLastModified(oldF2.lastModified());

        // create some "updated" (already installed, then touched) files
        updatedF1 = new File(instDir, "updated1.jar");
        assertTrue ("could not create updated file1", updatedF1.createNewFile());
        tsuF1 = new File(statusDir, "updated1.jar");
        assertTrue ("could not create timestamp file1", tsuF1.createNewFile());
        updatedF2 = new File(instDir, "updated2.jar");
        assertTrue ("could not create updated file2", updatedF2.createNewFile());
        tsuF2 = new File(statusDir, "updated2.jar");
        assertTrue ("could not create timestamp file2", tsuF2.createNewFile());
        tsuF1.setLastModified(updatedF1.lastModified() - 300);
        tsuF2.setLastModified(updatedF2.lastModified() - 300);

        File results = autoAdminTask.pollAutoDirectory(
                           AutoAdminTask.PollFunction.NEW_FILES, instDir, statusDir);
        assertFalse("pollAutoDirectory returned null -- expecting F1 or F2",
                    (results == null));

        boolean foundOne = results.equals(newF1);
        boolean foundTwo = results.equals(newF2);
        if (foundOne)
        {
            System.err.println("    (found F1)");
        }
        if (foundTwo)
        {
            System.err.println("    (found F2)");
        }
        assertTrue ("pollAutoDirectory found " + results.getName() +
                    " instead of F1 or F2.", foundOne | foundTwo);

        // remove the file, so we can find the other file
        assertTrue("could not remove file " + results.getName() + ".",
                    results.delete());

        // second call to pollAutoDirectory
        results = autoAdminTask.pollAutoDirectory(
                       AutoAdminTask.PollFunction.NEW_FILES, instDir, statusDir);
        foundOne = foundOne | newF1.equals(results);
        foundTwo = foundTwo | newF2.equals(results);
        assertTrue ("pollAutoDirectory found " + results.getName() +
                    " instead of F1 and F2.", foundOne & foundTwo);

        // remove the file, so we can be sure that we are done
        assertTrue("could not remove file " + results.getName() + ".",
                    results.delete());

        // third and last call to pollAutoDirectory
        results = autoAdminTask.pollAutoDirectory(
                       AutoAdminTask.PollFunction.NEW_FILES, instDir, statusDir);
        assertNull("pollAutoDirectory returned a non-null file", results);
    }

    // when phase 5 is completed, there will be a new Filled() test here
    // when phase 6 is completed, there will be a new Filled() test here

    /*
     * Test that pollAutoDirectory returns the right files under a complex
     * scenario with new and processed files in place.
     * @throws Exception when test fails for any reason.
     */
    public void testPollAutoDirectoryDeletedFilled()
        throws Exception
    {
        File instDir = new File(mAutoInstallDir);
        File statusDir = new File(mAutoInstallDir, ".autoinstallstatus");

        File results = autoAdminTask.pollAutoDirectory(
                           AutoAdminTask.PollFunction.DELETED_FILES, instDir, statusDir);
        assertFalse("pollAutoDirectory returned null -- expecting D1 or D2",
                    (results == null));

        boolean foundOne = results.equals(deletedF1);
        boolean foundTwo = results.equals(deletedF2);
        if (foundOne)
        {
            System.err.println("    (found D1)");
        }
        if (foundTwo)
        {
            System.err.println("    (found D2)");
        }
        assertTrue ("pollAutoDirectory found " + results.getName() +
                    " instead of D1 or D2.", foundOne | foundTwo);

        // remove the file, so we can find the other file
        assertTrue("could not remove file " + results.getName() + ".",
                    results.delete());

        // second call to pollAutoDirectory
        results = autoAdminTask.pollAutoDirectory(
                       AutoAdminTask.PollFunction.DELETED_FILES, instDir, statusDir);
        foundOne = foundOne | deletedF1.equals(results);
        foundTwo = foundTwo | deletedF2.equals(results);
        assertTrue ("pollAutoDirectory found " + results.getName() +
                    " instead of D1 and D2.", foundOne & foundTwo);

        // remove the file, so we can be sure that we are done
        assertTrue("could not remove file " + results.getName() + ".",
                    results.delete());

        // third and last call to pollAutoDirectory
        results = autoAdminTask.pollAutoDirectory(
                       AutoAdminTask.PollFunction.DELETED_FILES, instDir, statusDir);
        assertNull("pollAutoDirectory returned a non-null file", results);
    }

    /*
     * Test that pollAutoDirectory returns the right files under a complex
     * scenario with new and processed files in place.
     * @throws Exception when test fails for any reason.
     */
    public void testPollAutoDirectoryUpdatedFilled()
        throws Exception
    {
        File instDir = new File(mAutoInstallDir);
        File statusDir = new File(mAutoInstallDir, ".autoinstallstatus");

        File results = autoAdminTask.pollAutoDirectory(
                           AutoAdminTask.PollFunction.UPDATED_FILES, instDir, statusDir);
        assertFalse("pollAutoDirectory returned null -- expecting U1 or U2",
                    (results == null));

        boolean foundOne = results.equals(updatedF1);
        boolean foundTwo = results.equals(updatedF2);
        if (foundOne)
        {
            System.err.println("    (found U1)");
        }
        if (foundTwo)
        {
            System.err.println("    (found U2)");
        }
        assertTrue ("pollAutoDirectory found " + results.getName() +
                    " instead of U1 or U2.", foundOne | foundTwo);

        // remove the file, so we can find the other file
        assertTrue("could not remove file " + results.getName() + ".",
                    results.delete());

        // second call to pollAutoDirectory
        results = autoAdminTask.pollAutoDirectory(
                       AutoAdminTask.PollFunction.UPDATED_FILES, instDir, statusDir);
        foundOne = foundOne | updatedF1.equals(results);
        foundTwo = foundTwo | updatedF2.equals(results);
        assertTrue ("pollAutoDirectory found " + results.getName() +
                    " instead of U1 and U2.", foundOne & foundTwo);

        // remove the file, so we can be sure that we are done
        assertTrue("could not remove file " + results.getName() + ".",
                    results.delete());

        // third and last call to pollAutoDirectory
        results = autoAdminTask.pollAutoDirectory(
                       AutoAdminTask.PollFunction.UPDATED_FILES, instDir, statusDir);
        assertNull("pollAutoDirectory returned a non-null file", results);
    }

    /*
     * Test that exists to clean up the auto* directories
     * @throws Exception when test fails for any reason.
     */
    public void testCleanUp()
        throws Exception
    {
        File instDir = new File(mAutoInstallDir);
        File deplDir = new File(mAutoDeployDir);
        assertTrue("could not clean directory(" + mAutoInstallDir + ").",
                    FileHelper.cleanDirectory(instDir));
        assertTrue("could not delete directory(" + mAutoInstallDir + ").",
                    instDir.delete());
        assertTrue("could not clean directory(" + mAutoDeployDir + ").",
                    FileHelper.cleanDirectory(deplDir));
        assertTrue("could not delete directory(" + mAutoDeployDir + ").",
                    deplDir.delete());
    }

// the following methods of AutoApplyTask actually do perform installs and
// deploys (and require the App Server to be running), and so are not
// appropriate for testing with junit:
// performAutoDeploy
// performAutoInstall
// performAutoFunctions

}
