/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RegistryBuilder.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  RegistryBuilder.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on July 21, 2005, 5:14 PM
 */

package com.sun.jbi.management.registry;

import java.io.File;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class RegistryBuilder
{   
    /**
     * cache for various registries 
     */
    private static java.util.HashMap<String, Registry> sRegistryCache;
    
    static 
    {
        initCache();
    }
    
    /**
     * Creates a new instance of Registry based on the Registry Specification.
     *
     * @param spec a Registry specification.
     * @throws UnsupportedRegistryTypeException when asked to create a Registry 
     * of an unknown type.
     * @return a specific Registry Implementation based on the RegistrySpec.
     * @throws RegistryException if problems are encountered in building registry.
     */
    public static Registry buildRegistry(RegistrySpec spec)
        throws UnsupportedRegistryTypeException, RegistryException
    {
        String regFolder = spec.getProperties().getProperty(Registry.REGISTRY_FOLDER_PROPERTY);
            
        Registry registry = null;

        if ( spec.getType().equals( RegistryType.XML ) )
        {
            synchronized (sRegistryCache)
            {
                if ( !sRegistryCache.containsKey(regFolder) )
                {
                    registry = 
                        new com.sun.jbi.management.registry.xml.RegistryImpl(spec);

                    //spec.getManagementContext().setRegistry(registry);

                    sRegistryCache.put(regFolder, registry);
                }
                else
                {

                    registry = sRegistryCache.get(regFolder);
                }
                return registry;
            }
        }
        else
        {
            throw new UnsupportedRegistryTypeException(spec.getType().toString());
        }
    }
    
    /**
     * Destroy all the Registry Instances
     */
    public static void destroyRegistry()
    {
        synchronized (sRegistryCache)
        {
            java.util.Set<String> folderList = sRegistryCache.keySet();

            for ( String folder : folderList )
            {
                Registry registry = sRegistryCache.get(folder);
                registry.destroy();
                registry = null;
                sRegistryCache.remove(folder);
            }
        }
    }
    
 
    /**
     * Destroy a particular Registry Instance.
     */
    public static void destroyRegistry(String folder)
    {
        synchronized (sRegistryCache)
        {
            if ( sRegistryCache.containsKey(folder) )
            {
                Registry registry = sRegistryCache.get(folder);
                registry.destroy();
                registry = null;
                sRegistryCache.remove(folder);
            }   
        }
    }
    
    /**
     * Return the instance of the registry in the specified folder if one has been
     * initialized, return null otherwise. 
     */
    public static Registry getRegistryInstance(String folder)
    {
        synchronized (sRegistryCache)
        {
            if ( sRegistryCache.containsKey(folder) )
            {
                return sRegistryCache.get(folder);
            }
            else
            {
                return null;
            }
        }
    }
    
    /**
     * Initialize the registry cache
     */
    private static void initCache()
    {
        synchronized (RegistryBuilder.class)
        {
            sRegistryCache = new java.util.HashMap();
        }
    }
}
