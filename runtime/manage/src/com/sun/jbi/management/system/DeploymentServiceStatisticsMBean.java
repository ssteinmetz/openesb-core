/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeploymentServiceStatisticsMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.system;

import java.util.Date;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.OpenDataException;

/**
 * This class implements the MBean for collection of statistics for the
 * deployment service. All statistics are since the last deployment service startup;
 * they are all reset when the deployment service is restarted.
 *
 * @author Sun Microsystems, Inc.
 */
public interface DeploymentServiceStatisticsMBean 
{
    /**
     * Disable statistics collection. This method causes collection for this
     * object and all its child objects to be disabled.
     */
    public void setDisabled();
    
    /**
     * Enable statistics collection. This method causes collection for this
     * object and all its child objects to be disabled.
     */    
    public void setEnabled();
    
    /**
     * Check if statistics collection is enabled
     * @return true if enabled
     */
    public boolean isEnabled();

    /**
     * Get the time that the deployment service was last started.
     * @return The time of the last successful start() call.
     */
    Date getLastRestartTime();
    
    /**
     * This method is used to set the last restart time.
     * @param The time of the last successful init() call.
     */
    public void  setLastRestartTime(Date date);

    /**
     * Get the CompositeData instance that represents the current values for 
     * Service Assembly statistics.
     * @param serviceAssemblyName SA name
     * @return SA statistics in a CompositedData instance
     */
    CompositeData getServiceAssemblyStatistics(String serviceAssemblyName)
    throws OpenDataException, RuntimeException;
    
}
