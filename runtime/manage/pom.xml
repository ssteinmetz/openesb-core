<?xml version="1.0" encoding="UTF-8"?>
<!--
 # BEGIN_HEADER - DO NOT EDIT
 #
 # The contents of this file are subject to the terms
 # of the Common Development and Distribution License
 # (the "License").  You may not use this file except
 # in compliance with the License.
 #
 # You can obtain a copy of the license at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # See the License for the specific language governing
 # permissions and limitations under the License.
 #
 # When distributing Covered Code, include this CDDL
 # HEADER in each file and include the License file at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # If applicable add the following below this CDDL HEADER,
 # with the fields enclosed by brackets "[]" replaced with
 # your own identifying information: Portions Copyright
 # [year] [name of copyright owner]
-->

<!--
 # @(#)pom.xml
 # Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 #
 # END_HEADER - DO NOT EDIT
-->
<project>
    <parent>
        <artifactId>build-common</artifactId>
        <groupId>open-esb</groupId>
        <version>1.0</version>
        <relativePath>../../build-common</relativePath>
    </parent>
    <modelVersion>4.0.0</modelVersion>
    <groupId>open-esb</groupId>
    <artifactId>manage</artifactId>
    <name>management-services</name>
    <version>${openesb.currentVersion}</version>
    <description>JBI Runtime Management components, providing installation, deployment, and other JMX interfaces for remote management consoles.</description>
    <build>
        <resources>
            <resource>
                <directory>src</directory>
                <includes>
                    <include>**/*.properties</include>
                </includes>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <artifactId>maven-antrun-plugin</artifactId>
                <executions>
                    <execution>
                        <id>${project.artifactId}-generate-sources</id>
                        <phase>generate-sources</phase>
                        <goals><goal>run</goal></goals>
                        <configuration>
                            <tasks>
                                <ant antfile="manage.ant" dir="${basedir}" >
                                    <!-- junit system properties: -->
                                    <property name="jaxb.code.gen.schema.dir" value="${basedir}/src/schemas" />
                                    <property name="manage.basedir" value="${basedir}" />
                                    <property name="manage.bld.src.dir" value="${project.build.outputDirectory}" />
                                    <property name="openesb.currentVersion" value="${openesb.currentVersion}" />
                                    <reference refid="maven.test.classpath"/>                                    
                                </ant>
                            </tasks>
                        </configuration>
                    </execution>
                    <execution>
                        <id>${project.artifactId}-setup-packages</id>
                        <phase>package</phase>
                        <goals><goal>run</goal></goals>
                        <configuration>
                            <tasks>
                                <echo message="SET-UP ${project.artifactId} ${SECONDARY_ARTIFACT_ID} package..." />

                                <!-- copy the classes we want in the secondary jar: -->
                                <delete dir="${project.build.directory}/${SECONDARY_ARTIFACT_ID}" />
                                <mkdir  dir="${project.build.directory}/${SECONDARY_ARTIFACT_ID}" />
                                <echo message="Creating ${SECONDARY_ARTIFACT_ID}.jar." />
                                <copy todir="${project.build.directory}/${SECONDARY_ARTIFACT_ID}">
                                    <fileset dir="${project.build.outputDirectory}">
	<include name="com/sun/jbi/management/support/JbiNameInfo.class" />
	<include name="com/sun/jbi/management/engine/ModelEngineComponent.class" />
	<include name="com/sun/jbi/management/engine/ModelEngineInstaller.class" />
	<include name="com/sun/jbi/management/common/ControllerMBean.class" />
	<include name="com/sun/jbi/management/common/GenericConfigurationMBean.class" />
	<include name="com/sun/jbi/management/common/ConfigurationMBean.class" />
	<include name="com/sun/jbi/management/common/LoggerMBean.class" />
	<include name="com/sun/jbi/management/support/MBeanSet*.class" />
	<include name="com/sun/jbi/management/support/MBeanNamesImpl*.class" />
	<include name="com/sun/jbi/management/util/AppServerInfo.class" />
	<include name="com/sun/jbi/management/system/AdminEventBroadcaster.class" />
	<include name="com/sun/jbi/management/system/ManagementException.class" />
	<include name="com/sun/jbi/management/support/NotifyStandardMBean.class" />
	<include name="com/sun/jbi/management/support/ConfigurationSupport.class" />
	<include name="com/sun/jbi/management/binding/ModelBindingComponent.class" />
	<include name="com/sun/jbi/management/binding/ModelBindingInstaller.class" />
	<include name="com/sun/jbi/management/LocalStringKeys.class" />
	<include name="com/sun/jbi/management/LocalStrings.properties" />
                                    </fileset>
                                </copy>
                            </tasks>
                        </configuration>
                    </execution>
                    <execution>
                        <id>${project.artifactId}-ant-junit</id>
                        <phase>test</phase>
                        <goals><goal>run</goal></goals>
                        <configuration>
                            <tasks>
                                <patternset id="openesb.junit.test.patternset" >
                                    <include name="**/Test*.java"/>
                                    <exclude name="**/TestDeployment.java" />
                                </patternset>

                                <ant antfile="${project.parent.relativePath}/m2.ant"
                                    dir="${basedir}" target="run_junit" >
                                    <property name="maven.test.skip" value="${maven.test.skip}" />
                                    <property name="maven.repo.local" value="${maven.repo.local}" />
                                    <property name="project.artifactId" value="${project.artifactId}" />
                                    <property name="project.build.directory" value="${project.build.directory}" />
                                    <property name="project.build.testSourceDirectory" value="${project.build.testSourceDirectory}" />

                                    <!-- junit system properties: -->
                                    <property name="junit.srcroot" value="${SRCROOT}"/>
                                    <property name="junit.as8base" value="${AS8BASE}"/>
                                    <property name="com.sun.jbi.home" value="${JBI_HOME}"/>
                                    <propertyset id="openesb.junit.sysproperties">
                                        <propertyref name="junit.srcroot"/>
                                        <propertyref name="junit.as8base"/>
                                        <propertyref name="com.sun.jbi.home"/>
                                    </propertyset>

                                    <reference refid="maven.test.classpath"/>
                                    <reference refid="openesb.junit.sysproperties"/>
                                    <reference refid="openesb.junit.test.patternset"/>
                                </ant>
                            </tasks>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-jar-plugin</artifactId>
                <executions>
                    <execution>
                        <id>${project.artifactId}-secondary-jar</id>
                        <phase>package</phase>
                        <goals><goal>jar</goal></goals>
                        <configuration>
                            <classesDirectory>${project.build.directory}/${SECONDARY_ARTIFACT_ID}</classesDirectory>
                            <classifier>${SECONDARY_ARTIFACT_ID}</classifier>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>build-helper-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <id>${project.artifactId}-add-source</id>
                        <phase>generate-sources</phase>
                        <goals><goal>add-source</goal></goals>
                        <configuration>
                            <sources>
                                <source>${project.build.directory}/jaxb_code_gen/src</source>
                            </sources>
                        </configuration>
                    </execution> 
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
        </plugins>
    </build>
    <dependencies>
        <dependency>
            <groupId>open-esb</groupId>
            <artifactId>base</artifactId>
            <version>${openesb.currentVersion}</version>
        </dependency>
        <dependency>
            <groupId>open-esb</groupId>
            <artifactId>jbi</artifactId>
            <version>${openesb.currentVersion}</version>
        </dependency>
        <dependency>
            <groupId>open-esb</groupId>
            <artifactId>esb-util</artifactId>
            <version>${openesb.currentVersion}</version>
        </dependency>        
        <dependency>
            <groupId>jdmk</groupId>
            <artifactId>jdmkrt</artifactId>
        </dependency>
        <!-- GLASSFISH DEPENDENCIES: -->
        <dependency>
            <groupId>jbiplatform</groupId>
            <artifactId>jbi_compileconf</artifactId>
            <version>${openesb.currentVersion}</version>
        </dependency>

        <dependency>
            <groupId>open-esb</groupId>
            <artifactId>esb-util</artifactId>
            <version>${openesb.currentVersion}</version>
        </dependency>
        <dependency>
            <groupId>open-esb</groupId>
            <artifactId>jbi-admin-common</artifactId>
            <version>${openesb.currentVersion}</version>
        </dependency>

        <!-- TEST -->
        <dependency>
            <groupId>jmx4ant</groupId>
            <artifactId>jmx4ant</artifactId>
            <version>1.2b1</version>
            <scope>test</scope>
        </dependency>
    </dependencies>
    <properties>
        <SECONDARY_ARTIFACT_ID>sun-component-extensions</SECONDARY_ARTIFACT_ID>
    </properties>
</project>
