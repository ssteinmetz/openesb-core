/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeploymentServiceImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.impl.deployment;

import java.io.Serializable;
import java.util.Map;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;

import com.sun.esb.management.api.deployment.DeploymentService;
import com.sun.esb.management.base.services.BaseServiceImpl;
import com.sun.esb.management.common.ManagementRemoteException;

/**
 * Defines client operations for common deployment/undeployment services 
 * for the client
 * 
 * @author graj
 */
public class DeploymentServiceImpl extends BaseServiceImpl implements
        Serializable, DeploymentService {
    
    static final long serialVersionUID = -1L;
    
    /** Constructor - Constructs a new instance of DeploymentServiceImpl */
    public DeploymentServiceImpl() {
        super(null, false);
    }
    
    /**
     * Constructor - Constructs a new instance of DeploymentServiceImpl
     * 
     * @param serverConnection
     */
    public DeploymentServiceImpl(MBeanServerConnection serverConnection) {
        super(serverConnection, false);
    }
    
    /**
     * Constructor - Constructs a new instance of DeploymentServiceImpl
     * 
     * @param serverConnection
     * @param isRemoteConnection
     */
    public DeploymentServiceImpl(MBeanServerConnection serverConnection,
            boolean isRemoteConnection) {
        super(serverConnection, isRemoteConnection);
    }
    
    /**
     * deploys service assembly
     * 
     * @return result as a management message xml text
     * @param zipFilePath
     *            file path
     * @return result as a management message xml text string.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.deployment.DeploymentService#deployServiceAssembly(java.lang.String, java.lang.String)
     */
    public String deployServiceAssembly(String zipFilePath, String targetName)
            throws ManagementRemoteException {
        String result = null;
        ObjectName mbeanName = this.getDeploymentServiceMBeanObjectName();
        Object resultObject = null;
        
        if (this.isRemoteConnection() == true) {
            zipFilePath = uploadFile(zipFilePath);
        }
        
        Object[] params = new Object[2];
        params[0] = zipFilePath;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "deployServiceAssembly", params, signature);
        
        this.removeUploadedFile(); // if uploaded
        
        result = resultObject.toString();
        
        return result;
    }
    
    /**
     * deploys service assembly
     * 
     * @param zipFilePath
     *            fie path
     * @param targetNames
     * @return result as a management message xml text [targetName,xmlString]
     *         map
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.deployment.DeploymentService#deployServiceAssembly(java.lang.String, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> deployServiceAssembly(String zipFilePath,
            String[] targetNames) throws ManagementRemoteException {
        Map<String, String> result = null;
        ObjectName mbeanName = this.getDeploymentServiceMBeanObjectName();

        if (this.isRemoteConnection() == true) {
            zipFilePath = uploadFile(zipFilePath);
        }

        Object[] params = new Object[2];
        params[0] = zipFilePath;
        params[1] = targetNames;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = targetNames.getClass().getName();
        
        result = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "deployServiceAssembly", params, signature);
        
        this.removeUploadedFile(); // if uploaded

        return result;
    }
    
    /**
     * deploys service assembly from domain target
     * 
     * @param assemblyName
     *            service assembly name
     * @param targetName
     *            name of the target for this operation
     * @return result as a management message xml text string.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.deployment.DeploymentService#deployServiceAssemblyFromDomain(java.lang.String, java.lang.String)
     */
    public String deployServiceAssemblyFromDomain(String assemblyName,
            String targetName) throws ManagementRemoteException {
        String result = null;
        ObjectName mbeanName = this.getDeploymentServiceMBeanObjectName();
        
        Object[] params = new Object[2];
        params[0] = assemblyName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        result = (String) this.invokeMBeanOperation(mbeanName,
                "deployServiceAssemblyFromDomain", params, signature);
        
        return result;
    }
    
    /**
     * deploys service assembly
     * 
     * @param assemblyName
     *            name of the service assembly
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and management message xml text strings.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.deployment.DeploymentService#deployServiceAssemblyFromDomain(java.lang.String, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> deployServiceAssemblyFromDomain(
            String assemblyName, String[] targetNames)
            throws ManagementRemoteException {
        Map<String, String> result = null;
        ObjectName mbeanName = this.getDeploymentServiceMBeanObjectName();
        
        Object[] params = new Object[2];
        params[0] = assemblyName;
        params[1] = targetNames;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = targetNames.getClass().getName();
        
        result = (Map<String, String>) this
                .invokeMBeanOperation(mbeanName,
                        "deployServiceAssemblyFromDomain", params, signature);
        
        return result;
    }
    
    /**
     * forcibly undeploys service assembly with option to retain it in domain
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceDelete
     *            forces deletion of the assembly if true, false if not
     * @param retainInDomain
     *            true to not delete it from the domain target, false to also
     *            delete it from the domain target.
     * @param targetName
     *            name of the target for this operation
     * @return result as a management message xml text string.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.deployment.DeploymentService#undeployServiceAssembly(java.lang.String, boolean, boolean, java.lang.String)
     */
    public String undeployServiceAssembly(String serviceAssemblyName,
            boolean forceDelete, boolean retainInDomain, String targetName)
            throws ManagementRemoteException {
        String result = null;
        ObjectName mbeanName = this.getDeploymentServiceMBeanObjectName();
        
        Object[] params = new Object[4];
        params[0] = serviceAssemblyName;
        params[1] = Boolean.valueOf(forceDelete);
        params[2] = Boolean.valueOf(retainInDomain);
        params[3] = targetName;
        
        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "boolean";
        signature[2] = "boolean";
        signature[3] = "java.lang.String";
        
        result = (String) this.invokeMBeanOperation(mbeanName,
                "undeployServiceAssembly", params, signature);
        
        return result;
    }
    
    /**
     * forcibly undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceDelete
     *            forces deletion of the assembly if true, false if not
     * @param targetName
     *            name of the target for this operation
     * @return result as a management message xml text string.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.deployment.DeploymentService#undeployServiceAssembly(java.lang.String, boolean, java.lang.String)
     */
    public String undeployServiceAssembly(String serviceAssemblyName,
            boolean forceDelete, String targetName)
            throws ManagementRemoteException {
        String result = null;
        ObjectName mbeanName = this.getDeploymentServiceMBeanObjectName();
        
        Object[] params = new Object[3];
        params[0] = serviceAssemblyName;
        params[1] = Boolean.valueOf(forceDelete);
        params[2] = targetName;
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "boolean";
        signature[2] = "java.lang.String";
        
        result = (String)this.invokeMBeanOperation(mbeanName,
                "undeployServiceAssembly", params, signature);
        
        return result;
    }
    
    /**
     * forcibly undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetName
     *            name of the target for this operation
     * @return result as a management message xml text string.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.deployment.DeploymentService#undeployServiceAssembly(java.lang.String, java.lang.String)
     */
    public String undeployServiceAssembly(String serviceAssemblyName,
            String targetName) throws ManagementRemoteException {
        String result = null;
        ObjectName mbeanName = this.getDeploymentServiceMBeanObjectName();
        
        Object[] params = new Object[2];
        params[0] = serviceAssemblyName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        result =(String) this.invokeMBeanOperation(mbeanName,
                "undeployServiceAssembly", params, signature);
        
        return result;
    }
    
    /**
     * undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetNames
     * @return result as a management message xml text as [targetName, string]
     *         map
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.deployment.DeploymentService#undeployServiceAssembly(java.lang.String, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> undeployServiceAssembly(
            String serviceAssemblyName, String[] targetNames)
            throws ManagementRemoteException {
        Map<String, String> result = null;
        ObjectName mbeanName = this.getDeploymentServiceMBeanObjectName();
        
        Object[] params = new Object[2];
        params[0] = serviceAssemblyName;
        params[1] = targetNames;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = targetNames.getClass().getName();
        
        result = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "undeployServiceAssembly", params, signature);
        
        return result;
    }
    
    /**
     * undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceDelete
     *            true to delete, false to not
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and result as a management message xml text
     *         strings.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.deployment.DeploymentService#undeployServiceAssembly(java.lang.String, boolean, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> undeployServiceAssembly(
            String serviceAssemblyName, boolean forceDelete,
            String[] targetNames) throws ManagementRemoteException {
        ObjectName mbeanName = this.getDeploymentServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[3];
        params[0] = serviceAssemblyName;
        params[1] = forceDelete;
        params[2] = targetNames;
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "boolean";
        signature[2] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "undeployServiceAssembly", params, signature);
        
        return resultObject;
    }
    
    /**
     * undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceDelete
     *            forces deletion of the assembly if true, false if not
     * @param retainInDomain
     *            true to not delete it from the domain target, false to also
     *            delete it from the domain target.
     * @param targetNames
     *            array of targets for this operation
     * @return Map of targetName and result as a management message xml text
     *         strings.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.deployment.DeploymentService#undeployServiceAssembly(java.lang.String, boolean, boolean, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> undeployServiceAssembly(
            String serviceAssemblyName, boolean forceDelete,
            boolean retainInDomain, String[] targetNames)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getDeploymentServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[4];
        params[0] = serviceAssemblyName;
        params[1] = forceDelete;
        params[2] = retainInDomain;
        params[3] = targetNames;
        
        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "boolean";
        signature[2] = "boolean";
        signature[3] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "undeployServiceAssembly", params, signature);
        
        return resultObject;        
    }
    
}
