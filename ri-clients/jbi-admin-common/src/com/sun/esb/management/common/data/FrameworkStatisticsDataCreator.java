/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FrameworkStatisticsDataCreator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Set;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.esb.management.common.data.helper.FrameworkStatisticsDataReader;
import com.sun.jbi.ui.common.JBIRemoteException;

/**
 * @author graj
 *
 */
public class FrameworkStatisticsDataCreator {
    
    /** framework startup time */
    public static String FRAMEWORK_STARTUP_TIME            = "StartupTime";
    
    /** framework last restart time */
    public static String FRAMEWORK_LAST_RESTART_TIME       = "LastRestartTime";
    
    /** FrameworkStats CompositeType item names   */
    static String[]      FRAMEWORK_STATS_ITEM_NAMES        = { "InstanceName",
            "StartupTime", "UpTime"                       };
    
    /** FrameworkStats CompositeType item descriptions   */
    static String[]      FRAMEWORK_STATS_ITEM_DESCRIPTIONS = { "Instance Name",
            "Time taken to startup the framework (ms)",
            "Time elapsed since framework has been started (ms)" };
    
    /** FrameworkStats CompositeType item types   */
    static OpenType[]    FRAMEWORK_STATS_ITEM_TYPES        = {
            SimpleType.STRING, SimpleType.LONG, SimpleType.LONG };
    
    /** FrameworkStats table index   */
    static String[]      FRAMEWORK_STATS_TABLE_INDEX       = new String[] { "InstanceName" };
    
    /**
     * 
     */
    public FrameworkStatisticsDataCreator() {
    }
    
    /**
     * 
     * @param data
     * @return
     * @throws ManagementRemoteException
     */
    public static TabularData createTabularData(Map<String /* instanceName */, FrameworkStatisticsData> map)
            throws ManagementRemoteException {
        TabularData frameworkStatsTable = null;
        try {
            CompositeType frameworkStatsEntriesType = new CompositeType(
                    "FrameworkStatistics", "Framework Statistics",
                    FRAMEWORK_STATS_ITEM_NAMES,
                    FRAMEWORK_STATS_ITEM_DESCRIPTIONS,
                    FRAMEWORK_STATS_ITEM_TYPES);
            
            TabularType frameworkStatsType = new TabularType("FrameworkStats",
                    "Framework Statistic Information",
                    frameworkStatsEntriesType, FRAMEWORK_STATS_TABLE_INDEX);
            
            frameworkStatsTable = new TabularDataSupport(frameworkStatsType);
            Set<String> instances = map.keySet();
            for(String instanceName : instances) {
                FrameworkStatisticsData data = map.get(instanceName);
                frameworkStatsTable.put(getFrameworkStatistics(
                        frameworkStatsEntriesType, data));
            }
            
        } catch (OpenDataException e) {
            throw new ManagementRemoteException(e);
        }
        return frameworkStatsTable;
    }
    
    /**
     * This method is used to provide query framework statistics from 
     * the given data
     * @param frameworkStatsEntriesType
     * @param data
     * @return
     * @throws JBIRemoteException
     */
    protected static CompositeData getFrameworkStatistics(
            CompositeType frameworkStatsEntriesType,
            FrameworkStatisticsData data) throws ManagementRemoteException {
        try {
            Object[] values = { data.getInstanceName(), data.getStartupTime(),
                    data.getUpTime() };
            
            return new CompositeDataSupport(frameworkStatsEntriesType,
                    FRAMEWORK_STATS_ITEM_NAMES, values);
            
        } catch (Exception ex) {
            throw new ManagementRemoteException(ex);
        }
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        String uri = "C:/test/schema/frameworkstatistics/FrameworkStatisticsData.xml";
        try {
            Map<String /* instanceName */, FrameworkStatisticsData> map = null;
            map = FrameworkStatisticsDataReader.parseFromFile(uri);
            for (String instanceName : map.keySet()) {
                System.out.println(map.get(instanceName).getDisplayString());
            }
            TabularData data = FrameworkStatisticsDataCreator.createTabularData(map);
            System.out.println(data);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ManagementRemoteException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}
