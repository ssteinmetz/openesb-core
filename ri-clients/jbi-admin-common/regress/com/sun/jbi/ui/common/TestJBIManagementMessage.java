/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestJBIManagementMessage.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.common;

import junit.framework.TestCase;
import java.util.Map;
import java.util.HashMap;

/**
 * test class
 * @author Sun Microsystems, Inc.
 */
public class TestJBIManagementMessage extends TestCase
{
    private static String CANNED_SUCCESS_RESPONSE = 
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
           "<jbi-task version=\"1.0\" xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\">" +
                "<jbi-task-result><frmwk-task-result>" +
                            "<frmwk-task-result-details>" +
                                "<task-result-details>" +
                                        "<task-id>OPERATION</task-id>" +
                                        "<task-result>SUCCESS</task-result>" +
                                        "<message-type>INFO</message-type>" +
                                         "<task-status-msg>" +
                                            "<msg-loc-info>" +
                                               "<loc-token>JBIADMIN011</loc-token>" +
                                               "<loc-message>Success message.</loc-message>" +
                                               "<loc-param>Service Assembly</loc-param>" +
                                            "</msg-loc-info>" +
                                         "</task-status-msg>" +
                                "</task-result-details>" +
                                "<locale>en</locale>" +
                            "</frmwk-task-result-details>" +
                "</frmwk-task-result></jbi-task-result>" +
             "</jbi-task>";

 
    private static String CANNED_FAILURE_RESPONSE = 
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
           "<jbi-task version=\"1.0\" xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\">" +
                "<jbi-task-result><frmwk-task-result>" +
                            "<frmwk-task-result-details>" +
                                "<task-result-details>" +
                                        "<task-id>OPERATION</task-id>" +
                                        "<task-result>FAILED</task-result>" +
                                        "<message-type>ERROR</message-type>" +
                                         "<task-status-msg>" +
                                            "<msg-loc-info>" +
                                               "<loc-token>JBIADMIN012</loc-token>" +
                                               "<loc-message>Failure message.</loc-message>" +
                                               "<loc-param>Service Assembly</loc-param>" +
                                            "</msg-loc-info>" +
                                         "</task-status-msg>" +
                                "</task-result-details>" +
                                "<locale>en</locale>" +
                            "</frmwk-task-result-details>" +
                "</frmwk-task-result></jbi-task-result>" +
             "</jbi-task>";
    
    private static String CANNED_EXCEPTION_RESPONSE = 
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
           "<jbi-task version=\"1.0\" xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\">" +
                "<jbi-task-result><frmwk-task-result>" +
                            "<frmwk-task-result-details>" +
                                "<task-result-details>" +
                                        "<task-id>OPERATION</task-id>" +
                                        "<task-result>FAILED</task-result>" +
                                        "<message-type>ERROR</message-type>" +
                                         "<task-status-msg>" +
                                            "<msg-loc-info>" +
                                               "<loc-token>JBIADMIN013</loc-token>" +
                                               "<loc-message>Failure message.</loc-message>" +
                                               "<loc-param>Service Assembly</loc-param>" +
                                            "</msg-loc-info>" +
                                         "</task-status-msg>" +
                                         "<exception-info>" +
                                           "<nesting-level>1</nesting-level>" +
                                           "<msg-loc-info>" +
                                              "<loc-token>JBIADMIN014</loc-token>" +
                                              "<loc-message>First Instance Level Exception Message</loc-message>" +
                                           "</msg-loc-info>" +
                                           "<stack-trace>First Level Exception Stack Trace</stack-trace>" +
                                        "</exception-info>" +
                                "</task-result-details>" +
                                "<locale>en</locale>" +
                            "</frmwk-task-result-details>" +
                "</frmwk-task-result></jbi-task-result>" +
             "</jbi-task>";
    
    /**
     * Creates a new instance of TestMgmtMessage
     * @param aTestName name
     */
    public TestJBIManagementMessage(String aTestName )
    {
        super(aTestName);
    }
    
    /**
     * Test creating a composite management message
     * @throws Exception on error
     */
    public void testCreateJBIManagementMessage() 
        throws Exception
    {
        // Create a response map for two app vars
        Map<String, String> responseMap = new HashMap();
        responseMap.put("applicationVar1", CANNED_SUCCESS_RESPONSE);
        responseMap.put("applicationVar2", CANNED_SUCCESS_RESPONSE);
        
        // Create a management message
        JBIManagementMessage msg = JBIManagementMessage.createJBIManagementMessage(
            "addApplicationVariables", responseMap, false);
       
        assertTrue(msg.isSuccessMsg()); 
        assertTrue(msg.isInfoMsgType());
        assertEquals(4, msg.getFrameworkTaskResult().getTaskResultInfo().getStatusMessageInfoList().size());
    }
    
    /**
     * Test creating a composite management message
     * @throws Exception on error
     */
    public void testCreateJBIManagementMessageWithError() 
        throws Exception
    {
        // Create a response map for two app vars
        Map<String, String> responseMap = new HashMap();
        responseMap.put("applicationVar1", CANNED_SUCCESS_RESPONSE);
        responseMap.put("applicationVar2", CANNED_FAILURE_RESPONSE);
        
        // Create a management message
        JBIManagementMessage msg = JBIManagementMessage.createJBIManagementMessage(
            "addApplicationVariables", responseMap, false);
       
        assertTrue(msg.isWarningMsg()); 
        assertEquals(4, msg.getFrameworkTaskResult().getTaskResultInfo().getStatusMessageInfoList().size());
    }

    
    /**
     * Test creating a composite management message which has a exception info
     * @throws Exception on error
     */
    public void testCreateJBIManagementMessageWithExceptionInfo() 
        throws Exception
    {
        // Create a response map for two app vars
        Map<String, String> responseMap = new HashMap();
        responseMap.put("applicationVar1", CANNED_SUCCESS_RESPONSE);
        responseMap.put("applicationVar2", CANNED_FAILURE_RESPONSE);
        responseMap.put("applicationVar3", CANNED_EXCEPTION_RESPONSE);
        
        // Create a management message
        JBIManagementMessage msg = JBIManagementMessage.createJBIManagementMessage(
            "addApplicationVariables", responseMap, false);
        
        assertTrue(msg.isWarningMsg());
        
        assertEquals(6, msg.getFrameworkTaskResult().getTaskResultInfo().getStatusMessageInfoList().size());
        assertEquals(2, msg.getFrameworkTaskResult().getTaskResultInfo().getExceptionInfoList().size());
    }
    
    
    /**
     * Test creating a composite management message with requireAllSuccess=true
     * @throws Exception on error
     */
    public void testCreateJBIManagementMessageWithRequireAllSuccess() 
        throws Exception
    {
        // Create a response map for two app vars
        Map<String, String> responseMap = new HashMap();
        responseMap.put("applicationVar1", CANNED_SUCCESS_RESPONSE);
        responseMap.put("applicationVar2", CANNED_FAILURE_RESPONSE);
        
        // Create a management message
        JBIManagementMessage msg = JBIManagementMessage.createJBIManagementMessage(
            "addApplicationVariables", responseMap, true);
       
        assertTrue(msg.isFailedMsg()); 
        assertEquals(4, msg.getFrameworkTaskResult().getTaskResultInfo().getStatusMessageInfoList().size());
    }
    
    /**
     * Test creating a composite management message with a plain text exception message 
     * in the response for one application variable
     * @throws Exception on error
     */
    public void testCreateJBIManagementMessageWithExceptionMessage() 
        throws Exception
    {
        // Create a response map for two app vars
        Map<String, String> responseMap = new HashMap();
        responseMap.put("applicationVar1", CANNED_SUCCESS_RESPONSE);
        responseMap.put("applicationVar2", "Variable already exists in component");
        
        // Create a management message
        JBIManagementMessage msg = JBIManagementMessage.createJBIManagementMessage(
            "addApplicationVariables", responseMap, false);
       
        assertTrue(msg.isWarningMsg()); 
        assertEquals(4, msg.getFrameworkTaskResult().getTaskResultInfo().getStatusMessageInfoList().size());
    }
}
