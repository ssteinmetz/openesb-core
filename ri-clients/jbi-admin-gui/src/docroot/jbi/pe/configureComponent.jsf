<!--
 DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 
 Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 
 The contents of this file are subject to the terms of either the GNU
 General Public License Version 2 only ("GPL") or the Common Development
 and Distribution License("CDDL") (collectively, the "License").  You
 may not use this file except in compliance with the License. You can obtain
 a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 language governing permissions and limitations under the License.
 
 When distributing the software, include this License Header Notice in each
 file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 Sun designates this particular file as subject to the "Classpath" exception
 as provided by Sun in the GPL Version 2 section of the License file that
 accompanied this code.  If applicable, add the following below the License
 Header, with the fields enclosed by brackets [] replaced by your own
 identifying information: "Portions Copyrighted [year]
 [name of copyright owner]"
 
 Contributor(s):
 
 If you wish your version of this file to be governed by only the CDDL or
 only the GPL Version 2, indicate your decision by adding "[Contributor]
 elects to include this software in this distribution under the [CDDL or GPL
 Version 2] license."  If you don't indicate a single choice of license, a
 recipient has the option to distribute your version of this file under
 either the CDDL, the GPL Version 2 or to extend the choice of license to
 its licensees as provided above.  However, if you add GPL Version 2 code
 and therefore, elected the GPL Version 2 license, then the option applies
 only if the new code is made subject to such option by the copyright
 holder.
-->
<!-- jbi/pe/configureComponent.jsf -->

<sun:page>

    <!beforeCreate
//setJBIComponentId(JBIComponentName="$requestParameter{name}",JBIComponentType="$requestParameter{type}");
getRequestValue(key="name", value=>$attribute{name});
getRequestValue(key="type", value=>$attribute{type});
setJBIComponentId(JBIComponentName="$attribute{name}",JBIComponentType="$attribute{type}");
setResourceBundle(key="i18n" bundle="com.sun.jbi.jsf.resources.Bundle")
setResourceBundle(key="i18n2" bundle="com.sun.enterprise.tools.admingui.resources.Strings")
setResourceBundle(key="help" bundle="com.sun.enterprise.tools.admingui.resources.Helplinks");
setSessionAttribute(key="showNameLinkUrl", value="configureComponent.jsf")

<!-- Make sure the name and type session variables are set -->
if (!$session{sharedShowName}) {
    //setSessionAttribute(key="sharedShowName" value="$requestParameter{name}")
    setSessionAttribute(key="sharedShowName" value="$attribute{name}")
}
if (!$session{sharedShowType}) {
    //setSessionAttribute(key="sharedShowType" value="$requestParameter{type}")
    setSessionAttribute(key="sharedShowType" value="$attribute{type}")
}

jbiSetCompConfigData(
    compName="#{sessionScope.sharedShowName}"
    compType="#{sessionScope.sharedShowType}"
    configType="#{CompConfigBean.TYPE_RUNTIME}"
    prevSelectedInstance="#{sessionScope.jbiSelectedInstanceValue}"
    instancesList=>$page{instancesList}
    currSelectedInstance=>$session{jbiSelectedInstanceValue}
    isAlertNeeded=>$session{isJbiAlertNeeded}
    alertSummary=>$session{jbiAlertSummary}
    alertDetails=>$session{jbiAlertDetails}
    showStartButton=>$session{showStartButton}
    installedOnZeroTargets=>$session{installedOnZeroTargets}
)

if (!$session{jbiSelectedInstanceValue}){
    setSessionAttribute(key="jbiSelectedInstanceValue", value="server" );
}

if ($session{isJbiAlertNeeded}) {
   setSessionAttribute(key="showSaveButton" value="$boolean{false}")
}

if (!$session{isJbiAlertNeeded}) {
   setSessionAttribute(key="showSaveButton" value="$boolean{true}")
}
    />

    <sun:html>
        <sun:head id="jbiConfigureComponentHead" >
            "<script language="JavaScript" src="../../js/adminjsf.js"></script>
        </sun:head>

        <sun:body>
            <sun:form id="jbiConfigureComponentForm"> 
                <sun:hidden id="helpKey" value="$resource{help.jbi.pe.ConfigureComponent}" />
#include treeBreadcrumbs.inc
            </sun:form>

            <sun:form id="tabsForm">
#include "jbi/pe/inc/showTabs.inc"
            </sun:form>

#include "jbi/pe/inc/alertBox.inc"

            <sun:form id="configCompForm" >
     
                <sun:legend id="legend2" text="$resource{i18n2.required.field.legend}" style="padding-right: 8pt;"/>

                <sun:title id="title1"
                    title = "#{sessionScope.sharedShowName} - $resource{i18n.jbi.configuration.title}"
                    helpText="$resource{i18n.jbi.configuration.helpInline}"
                    style="text-align:left;">
    
                    <!-- Buttons  -->
                    <!facet pageButtonsTop>
                        <sun:panelGroup id="topButtons">
                            <sun:button id="saveButton" text="$resource{i18n2.button.Save}"                               
                                rendered="$session{showSaveButton}"> 
                                <!command
jbiSaveComponentConfiguration(
 compName="#{sessionScope.sharedShowName}"
 compType="#{sessionScope.sharedShowType}"
 configType="#{CompConfigBean.TYPE_RUNTIME}"
 currSelectedInstance="#{sessionScope.jbiSelectedInstanceValue}"
 isAlertNeeded=>$session{isJbiAlertNeeded}
 alertSummary=>$session{jbiAlertSummary}
 alertDetails=>$session{jbiAlertDetails}
);
redirect(page="configureComponent.jsf?name=#{sessionScope.sharedShowName}&type=#{sessionScope.sharedShowType}");
                                />
                            </sun:button>
                        </sun:panelGroup>
                    </facet>
                </sun:title>

                <sun:propertySheet 
                    id="propSheet" 
                    jumpLinks="true" 
                    style="text-align:left;" 
                    binding="#{CompConfigBean.compRuntimeCompConfigPS}"> 
                </sun:propertySheet>           	      		 

#include "jbi/pe/inc/startComponent.inc"


                <sun:title id="title2">
                    <!facet pageButtonsBottom>
                        <sun:panelGroup id="bottomButtons">
                            <sun:button id="saveButton2" text="$resource{i18n2.button.Save}" 
                              rendered="$session{showSaveButton}">                                                               
                                <!command
jbiSaveComponentConfiguration(
 compName="#{sessionScope.sharedShowName}"
 compType="#{sessionScope.sharedShowType}"
 configType="#{CompConfigBean.TYPE_RUNTIME}"
 currSelectedInstance="#{sessionScope.jbiSelectedInstanceValue}"
 isAlertNeeded=>$session{isJbiAlertNeeded}
 alertSummary=>$session{jbiAlertSummary}
 alertDetails=>$session{jbiAlertDetails}
);
redirect(page="configureComponent.jsf?name=#{sessionScope.sharedShowName}&type=#{sessionScope.sharedShowType}");
                                />
                            </sun:button>
                        </sun:panelGroup>
                    </facet>
                </sun:title>

            </sun:form>

        </sun:body> 
     
#include "changeButtonsJS.inc"           

     </sun:html>  
 </sun:page>

