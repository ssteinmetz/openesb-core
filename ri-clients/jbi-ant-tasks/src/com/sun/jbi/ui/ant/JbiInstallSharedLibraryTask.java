/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiInstallSharedLibraryTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import java.io.File;
import org.apache.tools.ant.BuildException;

/** This class is an ant task for installing a shared library.
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiInstallSharedLibraryTask extends JbiTargetTask
{
    /**
     * success msg key
     */
    private static final String SUCCESS_STATUS_KEY = "jbi.ui.ant.install.slib.successful";
    /**
     * failure msg key
     */
    private static final String FAILED_STATUS_KEY = "jbi.ui.ant.install.slib.failed";
    /** Holds value of property installFile. */
    private File mInstallFile;
    
    /** Holds value of property slib Name. */
    private String mSLibName = null; 
    
    /** Getter for property slib Name.
     * @return Value of property slib Name.
     *
     */
    public String getName()
    {
        return this.mSLibName;
    }
    
    /**
     * Setter for property slib Name.
     * @param name slib name.
     */
    public void setName(String name)
    {
        this.mSLibName = name;
    }
    
    /** Getter for property componentJar.
     * @return Value of property componentJar.
     *
     */
    public File getFile()
    {
        return this.mInstallFile;
    }
    
    /**
     * Setter for property componentJar.
     * @param file file object.
     */
    public void setFile(File file)
    {
        this.mInstallFile = file;
    }
    /**
     * validates the file
     */
    protected void validateInstallFile(File installFile) throws BuildException
    {
        if ( installFile == null )
        {
            throwTaskBuildException(
            "jbi.ui.ant.install.error.slib.archive.file.path.null");
        }
        
        if ( installFile.getPath().trim().length() <= 0 )
        {
            throwTaskBuildException(
            "jbi.ui.ant.install.error.slib.archive.file.path.required");
        }
        
        if ( !installFile.exists() )
        {
            
            throwTaskBuildException(
            "jbi.ui.ant.install.error.slib.archive.file.not.exist",
            installFile.getName());
        }
        
        if ( installFile.isDirectory() )
        {
            throwTaskBuildException(
            "jbi.ui.ant.install.error.comp.archive.file.is.directory");
        }
        
    }
    /**
     * validate compName with valid target
     * @param slibName name of the shared library in the repository
     * @param target value, can not be domain
     */
    protected void validateInstallFromDomainAttributes(String slibName, String target)  throws BuildException
    {
        if ( slibName.trim().length() == 0 )
        {
            throwTaskBuildException(
                "jbi.ui.ant.install.from.domain.error.slib.name.required");
        }
        if ( "domain".equals(target))
        {
            throwTaskBuildException(
                "jbi.ui.ant.install.error.slib.invalid.target.with.name.attrib");
        }
    }
    
    /** executes the install shared library task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {
        String installFileAbsolutePath = null;
        boolean installFromDoamin = false;
        
        String slibName = getName();
        
        File installFile = getFile();
        
        String target = getValidTarget();
        
        if ( slibName == null && installFile == null )
        {
            throwTaskBuildException(
                "jbi.ui.ant.install.error.slib.name.or.file.required");
        }
        
        if ( slibName != null && installFile != null )
        {
            throwTaskBuildException(
                "jbi.ui.ant.install.error.slib.name.and.file.set");
        }
        
        if ( slibName != null )
        {
            validateInstallFromDomainAttributes(slibName, target);
            installFromDoamin = true;
        }
        else
        {
            validateInstallFile(installFile);
            installFileAbsolutePath = installFile.getAbsolutePath();
        }
                        
        try
        {
            String result = null;
            
            if ( installFromDoamin )
            {
                this.getJBIAdminCommands().installSharedLibraryFromDomain(slibName, target);
            }
            else
            {
                result = this.getJBIAdminCommands().installSharedLibrary(installFileAbsolutePath, target);
            }
            
            printTaskSuccess(result);
            
        } catch (Exception ex )
        {
           // throwTaskBuildException(ex);
              processTaskException(ex);
        }
        
    }
    
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
        return FAILED_STATUS_KEY;
    }
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
        return SUCCESS_STATUS_KEY;
    }
    
}
