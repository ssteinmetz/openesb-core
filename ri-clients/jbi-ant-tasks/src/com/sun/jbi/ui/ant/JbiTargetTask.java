/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiTargetTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import com.sun.jbi.ui.client.ConnectionType;
import com.sun.jbi.ui.client.JBIAdminCommandsClientFactory;
import com.sun.jbi.ui.client.JMXConnectionProperties;
import com.sun.jbi.ui.common.I18NBundle;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jbi.ui.common.JBIManagementMessage;
import com.sun.jbi.ui.common.JBIRemoteException;
import com.sun.jbi.ui.common.JBIResultXmlBuilder;
import com.sun.jbi.ui.common.JMXConnectionException;
import com.sun.jbi.ui.common.JBIManagementMessage.JBIManagementMessageException;
import java.util.Properties;
import java.io.IOException;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;

import java.io.FileInputStream;
import java.io.File;

/** This class extends from the <@link JbiJmxTask> to add target attribute to the task.
 * Jbi tasks that required the target attribute in addition to the jmx connection attributes
 * will extends from this task. Jbi Tasks that don't need target attribute can directly 
 * extend from JbiJmxTask.
 *
 * @author Sun Microsystems, Inc.
 */

public abstract class JbiTargetTask extends JbiJmxTask
{
    /** server target */
    public static final String TARGET_SERVER = "server";
    /** domain target */
    public static final String TARGET_DOMAIN = "domain";
        
    /** Holds value of property Target. */
    private String mTarget = TARGET_SERVER;
            
    /** Getter for property target.
     * @return Value for Property target.
     */
    public String getTarget()
    {
        return this.mTarget;
    }
    
    /** Setter for property target.
     * @param target New value of property target.
     */
    public void setTarget(String target)
    {
        this.mTarget = target;
    }
    
    protected void validateTarget(String target) throws BuildException
    {
        if ( target != null && target.length() > 0 && target.trim().length() == 0 )
        {
            throwTaskBuildException("jbi.ui.ant.task.error.invalid.target", target);
        }
    }
    
    public String getValidTarget() throws BuildException
    {
        String target = getTarget();
        validateTarget(target);
        
        if ( target == null || target.length() == 0 )
        {
            target = TARGET_SERVER;
        }
        return target;
    }    
    
    protected void logDebugConnectionAttributes() {
        super.logDebugConnectionAttributes();
        logDebug("target=" + this.getTarget());
    }
    
}
