#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbi-admin-cli00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#


#----------------------------------------------------------------------------------
# Main function called that will run the test
#----------------------------------------------------------------------------------
run_test()
{
  initilize_test 
  
  echo "start-jbi-component sun-http-binding"
  $TEST_ASADMIN start-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS sun-http-binding
  
  echo "install-jbi-component fred"
  $TEST_ASADMIN install-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?
  
  echo "install-jbi-shared-library fred"
  $TEST_ASADMIN install-jbi-shared-library --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?
  
  echo "start-jbi-component fred"
  $TEST_ASADMIN start-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?
  
  echo "stop-jbi-component fred"
  $TEST_ASADMIN stop-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?
  
  echo "shut-down-jbi-component fred"
  $TEST_ASADMIN shut-down-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?
  
  echo "uninstall-jbi-component fred"
  $TEST_ASADMIN uninstall-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?
  
  echo "deploy-jbi-service-assembly fred"
  $TEST_ASADMIN deploy-jbi-service-assembly --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?
  
  echo "start-jbi-service-assembly fred"
  $TEST_ASADMIN start-jbi-service-assembly --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?
  
  echo "stop-jbi-service-assembly fred"
  $TEST_ASADMIN stop-jbi-service-assembly --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?
  
  echo "shut-down-jbi-service-assembly fred"
  $TEST_ASADMIN shut-down-jbi-service-assembly --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?
  
  echo "undeploy-jbi-service-assembly fred"
  $TEST_ASADMIN undeploy-jbi-service-assembly --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?
  
  echo "install-jbi-shared-library fred"
  $TEST_ASADMIN install-jbi-shared-library --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?
  
  echo "uninstall-jbi-shared-library fred"
  $TEST_ASADMIN uninstall-jbi-shared-library --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?
    
  echo "list-jbi-service-assemblies --target fred"
  $TEST_ASADMIN list-jbi-service-assemblies --port $ADMIN_PORT --target fred --user $ADMIN_USER $ASADMIN_PW_OPTS ; echo $?
  
  echo "list-jbi-binding-components --target fred"
  $TEST_ASADMIN list-jbi-binding-components --port $ADMIN_PORT --target fred --user $ADMIN_USER $ASADMIN_PW_OPTS ; echo $?
  
  echo "list-jbi-service-engine --target fred"
  $TEST_ASADMIN list-jbi-service-engine --port $ADMIN_PORT --target fred --user $ADMIN_USER $ASADMIN_PW_OPTS ; echo $?
  
  echo "list-jbi-shared-libraries --target fred"
  $TEST_ASADMIN list-jbi-shared-libraries --port $ADMIN_PORT --target fred --user $ADMIN_USER $ASADMIN_PW_OPTS ; echo $?
  
  echo "show-jbi-service-engine fred"
  $TEST_ASADMIN show-jbi-service-engine --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?

  echo "show-jbi-binding-component fred"
  $TEST_ASADMIN show-jbi-binding-component --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?

  echo "show-jbi-service-assembly fred"
  $TEST_ASADMIN show-jbi-service-assembly --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?

  echo "show-jbi-shared-library fred"
  $TEST_ASADMIN show-jbi-shared-library --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?
  
  echo "upgrade-jbi-component fred"
  $TEST_ASADMIN upgrade-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS --upgradefile=fred cli_test_engine4 ; echo $?
  
  echo "set-jbi-runtime-logger fred.com.sun.jbi.messaging=WARNING"
  $TEST_ASADMIN set-jbi-runtime-logger --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS fred.com.sun.jbi.messaging=WARNING ; echo $?
  
  echo "set-jbi-component-logger fred=INFO"
  $TEST_ASADMIN set-jbi-component-logger --component=sun-http-binding --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS fred=INFO ; echo $?
  
  echo "set-jbi-component-logger sun-http-binding.com.sun.jbi.httpsoapbc.HttpSoapBindingDeployer=fred"
  $TEST_ASADMIN set-jbi-component-logger --component=sun-http-binding --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS sun-http-binding.com.sun.jbi.httpsoapbc.HttpSoapBindingDeployer=fred ; echo $?
  
  echo "set-jbi-runtime-configuration heartBeatInterval-fred=6000"
  $TEST_ASADMIN set-jbi-runtime-configuration --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS heartBeatInterval-fred=6000 ; echo $?  
   
  echo "set-jbi-component-configuration OutboundThreads=fred"
  $TEST_ASADMIN set-jbi-component-configuration --component=sun-http-binding --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS OutboundThreads=fred ; echo $?
   
  echo "set-jbi-component-configuration fred=5"
  $TEST_ASADMIN set-jbi-component-configuration --component=sun-http-binding --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS fred=5 ; echo $?
   
  echo "update-jbi-application-configuration fred"
  $TEST_ASADMIN update-jbi-application-configuration --configname=fred --component=sun-http-binding --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?
     
  echo "update-jbi-application-variable bogusname=5"
  $TEST_ASADMIN update-jbi-application-variable --component=sun-http-binding --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS fred=5 ; echo $?
         
  echo "create-jbi-application-configuration fred"
  $TEST_ASADMIN create-jbi-application-configuration --configname=fred --component=sun-http-binding --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?
     
  echo "create-jbi-application-variable fred"
  $TEST_ASADMIN create-jbi-application-variable --component=sun-http-binding --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?
      
  echo "delete-jbi-application-variable dummy-component FirstName,LastName"
  $TEST_ASADMIN delete-jbi-application-variable --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS dummy-component FirstName,LastName ; echo $?
        
  echo "delete-jbi-application-configuration FirstName,LastName"
  $TEST_ASADMIN delete-jbi-application-configuration --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS --component=sun-http-binding testConfigBad ; echo $?
        
  echo "list-jbi-application-configurations --component=fred"
  $TEST_ASADMIN list-jbi-application-configurations --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS --component=fred ; echo $?
    
  echo "list-jbi-application-variable --component=fred"
  $TEST_ASADMIN list-jbi-application-variable --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS --component=fred ; echo $?
   
  echo "show-jbi-runtime-loggers --target fred"
  $TEST_ASADMIN show-jbi-runtime-loggers --port $ADMIN_PORT --target fred --user $ADMIN_USER $ASADMIN_PW_OPTS ; echo $?
      
  echo "show-jbi-runtime-configuration --target fred"
  $TEST_ASADMIN show-jbi-runtime-configuration --port $ADMIN_PORT --target fred --user $ADMIN_USER $ASADMIN_PW_OPTS ; echo $?
   
  echo "show-jbi-application-configuration fred"
  $TEST_ASADMIN show-jbi-application-configuration --component=sun-http-binding --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?
   
  echo "show-jbi-statistics --serviceassembly=fred"
  $TEST_ASADMIN show-jbi-statistics --serviceassembly=fred --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS ; echo $?
 
  echo "show-jbi-statistics --component=fred"
  $TEST_ASADMIN show-jbi-statistics --component=fred --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS ; echo $?
 
  echo "show-jbi-statistics --endpoint=fred"
  $TEST_ASADMIN show-jbi-statistics --endpoint=fred --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS ; echo $?
 
  echo "show-jbi-statistics --framework --target fred"
  $TEST_ASADMIN show-jbi-statistics --nmr --port $ADMIN_PORT --target fred --user $ADMIN_USER $ASADMIN_PW_OPTS ; echo $?
  
  echo "show-jbi-statistics --nmr --target fred"
  $TEST_ASADMIN show-jbi-statistics --nmr --port $ADMIN_PORT --target fred --user $ADMIN_USER $ASADMIN_PW_OPTS ; echo $?
 
  echo "show-jbi-statistics --component=sun-http-binding --target fred"
  $TEST_ASADMIN show-jbi-statistics --component=sun-http-binding --port $ADMIN_PORT --target fred --user $ADMIN_USER $ASADMIN_PW_OPTS ; echo $?
 
  echo "show-jbi-statistics --endpoint=abc --target fred"
  $TEST_ASADMIN show-jbi-statistics --endpoint=abc --port $ADMIN_PORT --target fred --user $ADMIN_USER $ASADMIN_PW_OPTS ; echo $?
 
  echo "show-jbi-statistics --serviceassembly=PingApp --target fred"
  $TEST_ASADMIN show-jbi-statistics --serviceassembly=fred --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS ; echo $?
 
  echo "export-jbi-application-environment fred"
  $TEST_ASADMIN export-jbi-application-environment --configdir=xyz  --port $ADMIN_PORT --target server --user $ADMIN_USER $ASADMIN_PW_OPTS fred ; echo $?
               
}


#----------------------------------------------------------------------------------
# Perform the regression setup and call the function that will run the test
#----------------------------------------------------------------------------------
TEST_NAME="jbi-admin-cli00016"
TEST_DESCRIPTION="Test exit code"
. ./regress_defs.ksh
run_test

exit 0


